package ru.reshala.promocodeservice

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.cloud.openfeign.EnableFeignClients

@SpringBootApplication()
@EnableFeignClients
@EnableEurekaClient
class PromocodeApplication

fun main(args: Array<String>) {
    SpringApplication.run(PromocodeApplication::class.java, *args)
}

