package ru.reshala.promocodeservice.service.interfaces

import ru.reshala.promocodeservice.model.UserPromocode
import java.util.*

interface UserPromocodeService {
    fun save(code: String): UserPromocode
    fun update(userPromocode: UserPromocode): UserPromocode
    fun getById(userPromocodeId:Long): UserPromocode?
    fun getAllByUserId(userId: UUID): List<UserPromocode>
    fun getByUserIdAndPromocodeCode(userId: UUID, promocodeCode: String): UserPromocode?
    fun getAll(): List<UserPromocode>
    fun use(code: String): UserPromocode
    fun save(code: String, userId: UUID): UserPromocode
}
