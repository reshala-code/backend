package ru.reshala.promocodeservice.service.interfaces

import ru.reshala.promocodeservice.model.Promocode

interface PromocodeService {
    fun save(promocode: Promocode): Promocode
    fun update(promocode: Promocode): Promocode
    fun getById(promocodeId: Long): Promocode?
    fun getByCode(code: String): Promocode?
    fun getAll(): List<Promocode>
}
