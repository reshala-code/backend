package ru.reshala.promocodeservice.service.impl

import org.springframework.stereotype.Service
import ru.reshala.promocodeservice.exception.BadRequestException
import ru.reshala.promocodeservice.exception.NotFoundException
import ru.reshala.promocodeservice.model.PromocodeType
import ru.reshala.promocodeservice.model.UserPromocode
import ru.reshala.promocodeservice.repository.PromocodeRepository
import ru.reshala.promocodeservice.repository.UserPromocodeRepository
import ru.reshala.promocodeservice.service.interfaces.UserPromocodeService
import ru.reshala.promocodeservice.utils.CurrentUser
import java.time.LocalDateTime
import java.util.UUID

@Service
class UserPromocodeServiceImpl(
    val userPromocodeRepository: UserPromocodeRepository,
    val promocodeRepository: PromocodeRepository,
) : UserPromocodeService {

    override fun getById(userPromocodeId: Long) =
        userPromocodeRepository.findOneById(userPromocodeId)

    override fun getAllByUserId(userId: UUID) =
        userPromocodeRepository.findAllByUserId(userId)

    override fun getByUserIdAndPromocodeCode(userId: UUID, promocodeCode: String) =
        userPromocodeRepository.findByUserIdAndPromocode_Code(userId, promocodeCode)

    override fun getAll() = userPromocodeRepository.findAll()

    override fun use(code: String): UserPromocode {
        promocodeRepository.findOneByCode(code) ?: throw NotFoundException("Промокод не найден")
        val userPromocode = userPromocodeRepository.findByUserIdAndPromocode_Code(CurrentUser.userIdAsUUID, code )?:save(code)
        validate(userPromocode)

        return userPromocode
    }

    override fun save(code: String): UserPromocode {
        val promocode = promocodeRepository.findOneByCode(code) ?: throw NotFoundException("Промокод не найден")
        val userPromocode = UserPromocode(userId = CurrentUser.userIdAsUUID, promocode = promocode)
        validate(userPromocode)

        userPromocode.isActivated = true


        return userPromocodeRepository.save(userPromocode)
    }

    override fun save(code: String, userId: UUID): UserPromocode {
        val promocode = promocodeRepository.findOneByCode(code) ?: throw NotFoundException("Промокод не найден")
        val userPromocode = UserPromocode(userId = userId, promocode = promocode)
        validate(userPromocode)

        userPromocode.isActivated = true


        return userPromocodeRepository.save(userPromocode)
    }

    override fun update(userPromocode: UserPromocode): UserPromocode {
        val promocodeInDB = userPromocodeRepository.findOneById(userPromocode.id)
            ?: throw NotFoundException("Такой промокод не найден")

        if (userPromocode.usagesLeft != null && userPromocode.usagesLeft!! < 0) {
            throw BadRequestException("Количество использований не может быть меньше 0")
        }

        promocodeInDB.usagesLeft = userPromocode.usagesLeft

        return userPromocodeRepository.save(promocodeInDB)
    }

    private fun validate(userPromocode: UserPromocode) {
        if (!promocodeRepository.existsByCode(userPromocode.promocode.code)) {
            throw NotFoundException("Промокод не найден")
        }

//        if (userPromocodeRepository.existsByUserIdAndPromocode(userPromocode.userId, userPromocode.promocode) && userPromocode.promocode.type!=PromocodeType.COMMON) {
//
//            throw BadRequestException("Промокод уже использован")
//        }

        if (userPromocode.promocode.type == PromocodeType.INDIVIDUAL && userPromocodeRepository.existsByPromocode(userPromocode.promocode)) {
            throw NotFoundException("Промокод не найден")
        }

        if (userPromocode.promocode.expireTime!!.isBefore(LocalDateTime.now())) {
            throw BadRequestException("Срок действия промокода истек")
        }

        if (userPromocode.usagesLeft!= null && userPromocode.usagesLeft!! <1){
            throw BadRequestException("Данный промокод уже нельзя использовать")
        }
    }


}
