package ru.reshala.promocodeservice.service.impl

import org.springframework.stereotype.Service
import ru.reshala.promocodeservice.exception.BadRequestException
import ru.reshala.promocodeservice.exception.NotFoundException
import ru.reshala.promocodeservice.model.Promocode
import ru.reshala.promocodeservice.model.PromocodeUnit
import ru.reshala.promocodeservice.repository.PromocodeRepository
import ru.reshala.promocodeservice.service.interfaces.PromocodeService

@Service
class PromocodeServiceImpl(val promocodeRepository: PromocodeRepository): PromocodeService {
    override fun getById(promocodeId: Long) = promocodeRepository.findOneById(promocodeId)

    override fun getByCode(code: String) = promocodeRepository.findOneByCode(code)

    override fun getAll() =  promocodeRepository.findAll()

    override fun save(promocode: Promocode): Promocode {
        validate(promocode)
        return promocodeRepository.save(promocode)
    }

    override fun update(promocode: Promocode): Promocode {
        val promocodeInDB = promocodeRepository.findOneById(promocode.id)
                ?: throw NotFoundException("Такой промокод не найден")

        promocodeInDB.expireTime = promocode.expireTime
        promocodeInDB.taskCategories = promocode.taskCategories

        return promocodeRepository.save(promocodeInDB)
    }

    private fun validate(promocode: Promocode){
        if (promocodeRepository.existsByCode(promocode.code)){
            throw BadRequestException("Промокод с таким кодом уже существует")
        }

        if (promocode.unit == PromocodeUnit.PERCENT && (promocode.value <1 || promocode.value > 100)){
            throw BadRequestException("В случае скидки в процентах, значение не может быть больше 100 или меньше 1")
        }

        if (promocode.value<1){
            throw BadRequestException("В случае скидки в рублях, скидка не может быть меньше 1")
        }

        if(promocode.maxValue!= null && promocode.maxValue <1){
            throw BadRequestException("Макимальное ограничение по скидке не может быть меньше 1")
        }

        if(promocode.usages!= null && promocode.usages <1){
            throw BadRequestException("Количество использований не может быть меньше 1")
        }
    }
}
