package ru.reshala.promocodeservice.controller

import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.*
import ru.reshala.promocodeservice.constants.SecurityConstants.ROLE_ADMIN
import ru.reshala.promocodeservice.model.UserPromocode
import ru.reshala.promocodeservice.service.interfaces.UserPromocodeService
import java.util.*

@RestController
@RequestMapping("/user-promocode")
@CrossOrigin
class UserPromocodeController(
    val userPromocodeService: UserPromocodeService,
) {
    @Secured(ROLE_ADMIN)
    @ApiOperation("Получить список промокодов пользователей")
    @GetMapping
    fun getAll() = userPromocodeService.getAll()

    @Secured(ROLE_ADMIN)
    @ApiOperation("Получить список промокодов принадлежащие пользователю по его ID")
    @GetMapping("/{userId}")
    fun getAllByUserId(@ApiParam("ID пользователя") @PathVariable userId: UUID) =
        userPromocodeService.getAllByUserId(userId)


//    @GetMapping("/{id}")
//    fun getById(@PathVariable id: Long) = userPromocodeService.getById(id)

    @ApiOperation("Получить информацию о промокоде по коду промокода и ID пользователя")
    @GetMapping("/{userId}/{code}")
    fun getByUserIdAndPromocodeCode(
        @ApiParam("ID пользователя")
        @PathVariable userId: UUID,
        @ApiParam("код программы") @PathVariable("code") promocodeCode: String,
    ) =
        userPromocodeService.getByUserIdAndPromocodeCode(userId, promocodeCode)

    @ApiOperation("Произвести попытку использовать промокод с проверкой его валидности")
    @PostMapping("/using")
    fun use(
        @ApiParam("Код промокода")
        @RequestBody code: String,
    ) = userPromocodeService.use(code)

    @ApiOperation("Активировать промокод для пользователя")
    @PostMapping
    fun save(
        @ApiParam("Код промокода")
        @RequestBody code: String,
    ) = userPromocodeService.save(code)

    @ApiOperation("Активировать промокод для пользователя с указанным id")
    @PostMapping("/{userId}")
    fun addPromocodeToUser(
        @ApiParam("Код промокода")
        @RequestBody code: String,
        @PathVariable("userId") userId: UUID
    ) = userPromocodeService.save(code,userId)

    @ApiOperation("Изменить информацию о промкоде пользователя",
        notes = "Используется для изменения оставшегося числа использований промокода")
    @PatchMapping
    fun update(
        @ApiParam("Обновленный объект промокода пользователя")
        @RequestBody userPromocode: UserPromocode,
    ) = userPromocodeService.update(userPromocode)
}

