package ru.reshala.promocodeservice.controller

import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.*
import ru.reshala.promocodeservice.constants.SecurityConstants.ROLE_ADMIN
import ru.reshala.promocodeservice.model.Promocode
import ru.reshala.promocodeservice.service.interfaces.PromocodeService

@RestController
@RequestMapping("/promocode")
@CrossOrigin
class PromocodeController(
    val promocodeService: PromocodeService,
) {
    @Secured(ROLE_ADMIN)
    @ApiOperation("Получить все промокоды")
    @GetMapping
    fun getAll() = promocodeService.getAll()

    @ApiOperation("Получить промокод по ID")
    @GetMapping("/id/{id}")
    fun getById(
        @ApiParam("ID промокода")
        @PathVariable id: Long,
    ) = promocodeService.getById(id)

    @ApiOperation("Получить информацию о промокоде")
    @GetMapping("/code/{code}")
    fun getByCode(@PathVariable code: String) = promocodeService.getByCode(code)

    @Secured(ROLE_ADMIN)
    @ApiOperation("Добавить промокод")
    @PostMapping
    fun save(
        @ApiParam("Объект промокода")
        @RequestBody promocode: Promocode,
    ) = promocodeService.save(promocode)

    @Secured(ROLE_ADMIN)
    @ApiOperation("Обновить данные о промокоде")
    @PatchMapping
    fun update(
        @ApiParam("Объект промокода")
        @RequestBody promocode: Promocode) = promocodeService.update(promocode)
}

