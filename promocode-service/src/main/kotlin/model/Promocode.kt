package ru.reshala.promocodeservice.model

import java.time.LocalDateTime
import javax.persistence.*
import javax.validation.constraints.Min

@Entity(name = "promocode")
data class Promocode(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0,
    @Enumerated(EnumType.STRING)
    val type: PromocodeType = PromocodeType.INDIVIDUAL,
    var expireTime: LocalDateTime? = LocalDateTime.now().plusMonths(1),
    var promocodeDescription: String,
    @Column(unique = true)
    val code: String,
    @Enumerated(EnumType.STRING)
    val unit: PromocodeUnit,
    @Min(1)
    val value: Int,
    @Min(1)
    val maxValue: Int? = null,
    @Min(1)
    val usages: Int? = null,
    @Convert(converter = TaskCategorySetToStringConverter::class)
    var taskCategories: Set<TaskCategory> = setOf(TaskCategory.ALL)
)

enum class TaskCategory {
    ALL,  COURIER, CLEANER, LOADER, GARBAGE
}

enum class PromocodeUnit {
    PERCENT, RUB
}

enum class PromocodeType {
    INDIVIDUAL, COMMON
}
