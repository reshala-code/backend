package ru.reshala.promocodeservice.model

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.readValue
import javax.annotation.PostConstruct
import javax.persistence.AttributeConverter
import javax.persistence.Converter

@Converter
class TaskCategorySetToStringConverter:  AttributeConverter<Set<TaskCategory>, String>  {
    val objectMapper = ObjectMapper()

    @PostConstruct
    fun setUp() {
        objectMapper.registerModule(JavaTimeModule())
    }

    override fun convertToDatabaseColumn(attribute: Set<TaskCategory>?): String {
        return objectMapper.writeValueAsString(attribute);
    }

    override fun convertToEntityAttribute(dbData: String?): Set<TaskCategory> {
        return objectMapper.readValue(dbData!!)
    }
}
