package ru.reshala.promocodeservice.model

import java.util.UUID
import javax.persistence.*
import javax.validation.constraints.Min

@Entity(name = "user_promocode")
data class UserPromocode(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0,
    val userId: UUID,
    @ManyToOne
    @JoinColumn(name = "promocode_id")
    val promocode: Promocode,
    @Min(1)
    var usagesLeft: Int? = promocode.usages,
    var isActivated: Boolean = false,
)
