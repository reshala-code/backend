package ru.reshala.promocodeservice.exception

class ForbiddenException: PromocodeServiceException {
    constructor(message: String): super(message)
    constructor(message: String, exception: Exception): super(message, exception)
}
