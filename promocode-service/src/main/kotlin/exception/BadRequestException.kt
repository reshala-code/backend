package ru.reshala.promocodeservice.exception

class BadRequestException : PromocodeServiceException {
    constructor(message: String) : super(message)
    constructor(message: String, exception: Exception) : super(message, exception)
}
