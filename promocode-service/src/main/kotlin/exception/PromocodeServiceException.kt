package ru.reshala.promocodeservice.exception

abstract class PromocodeServiceException: RuntimeException {
    constructor(message: String): super(message)
    constructor(message: String, exception: Exception): super(message, exception)
}
