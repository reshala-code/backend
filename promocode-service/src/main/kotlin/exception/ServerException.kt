package ru.reshala.promocodeservice.exception

class ServerException: PromocodeServiceException {
    constructor(message: String): super(message)
    constructor(message: String, exception: Exception): super(message, exception)
}
