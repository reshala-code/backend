package ru.reshala.promocodeservice.exception

class NotFoundException: PromocodeServiceException {
    constructor(message: String): super(message)
    constructor(message: String, exception: Exception): super(message, exception)
}
