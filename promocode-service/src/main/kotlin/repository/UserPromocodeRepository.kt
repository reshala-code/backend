package ru.reshala.promocodeservice.repository

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import ru.reshala.promocodeservice.model.Promocode
import ru.reshala.promocodeservice.model.UserPromocode
import java.util.*

@Repository
interface UserPromocodeRepository : PagingAndSortingRepository<UserPromocode,Long>{
    fun findOneById(id: Long): UserPromocode?
    fun findByUserIdAndPromocode_Code(userId: UUID, promocodeCode: String): UserPromocode?
    fun findAllByUserId(userId: UUID): List<UserPromocode>
    fun existsByUserIdAndPromocode(userId: UUID, promocode: Promocode): Boolean
    fun existsByPromocode(promocode: Promocode): Boolean
    override fun findAll(): List<UserPromocode>
}
