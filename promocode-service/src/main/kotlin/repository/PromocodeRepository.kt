package ru.reshala.promocodeservice.repository

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import ru.reshala.promocodeservice.model.Promocode

@Repository
interface PromocodeRepository : PagingAndSortingRepository<Promocode,Long>{
    fun findOneById(id: Long): Promocode?
    fun findOneByCode(code: String): Promocode?
    fun existsByCode(code: String):  Boolean
    override fun findAll(): List<Promocode>
}
