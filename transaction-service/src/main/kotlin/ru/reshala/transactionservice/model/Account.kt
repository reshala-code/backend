package ru.reshala.transactionservice.model

import ru.reshala.transactionservice.utils.CurrentUser
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Id

@Entity(name = "account")
data class Account (
        @Id
        val id: UUID = UUID.randomUUID(),
        @Column(unique = true)
        val userId: UUID? = null,
        var balance: Double = 0.0,
        @Enumerated(EnumType.STRING)
        val type: AccountType = AccountType.USER_ACCOUNT
){
        constructor() : this(UUID.randomUUID()) {
        }
}

enum class AccountType{
    USER_ACCOUNT, RESHALA_ACCOUNT
}
