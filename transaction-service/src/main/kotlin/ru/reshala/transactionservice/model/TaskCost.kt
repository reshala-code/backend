package ru.reshala.transactionservice.model

data class TaskCost(
    val cost: Int,
    val finalCost: Int,
)
