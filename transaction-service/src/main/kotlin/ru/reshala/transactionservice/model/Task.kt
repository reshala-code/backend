package ru.reshala.transactionservice.model

import java.util.*

class Task(
        var id: Long = 0,
        var cost: Double?,
        var addresses: List<Address>,
        var implementerId: UUID?,
        var creatorId: UUID?,
        var comment: String?,
        var type: TaskCategoryName,
        var categoryInfo: Any?,
        var refusedWorkers: MutableList<UUID> = mutableListOf(),
        var promocode: String? = null
)

enum class TaskCategoryName {
    COURIER, CLEANER, LOADER, GARBAGE
}

data class Address(
        val addressName: String?,
        var latitude: Double,
        var longitude: Double,
        var isChecked: Boolean = false,
)

data class CleanerCategoryInfo(
        val squareMeters: Double,
        val isCleaningWindowOn: Boolean = false
)
data class LoaderCategoryInfo(
        val minutes: Double
)

data class GarbageCategoryInfo(
        val bagCount: Double
)
