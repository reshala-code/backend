package ru.reshala.transactionservice.model

import org.springframework.data.annotation.CreatedDate
import ru.reshala.transactionservice.model.TransactionStatus.OPEN
import java.time.LocalDateTime
import java.util.UUID
import javax.persistence.*

@Entity(name = "money_transactions")
data class Transaction (
        @Id
        val id: UUID = UUID.randomUUID(),
        @Column(name = "from_account")
        var from: String? = null,
        @Column(name = "to_account")
        var to: String? = null,
        val entityId: String? = null,
        val description: String? = null,
        val commission: Double?  = null,
        val value: Double,
        val finalValue: Double,

        @Enumerated(EnumType.STRING)
        val type: TransactionType,

        @Enumerated(EnumType.STRING)
        var status: TransactionStatus = OPEN,

        @CreatedDate
        val createdDate: LocalDateTime = LocalDateTime.now()
)

enum class TransactionType(val hasCommission: Boolean = false) {
    TASK_PAY(true), SERVICE_PAY, DEPOSIT, WITHDRAW(true), COMMISSION, CORRECTION(true), WORKER_PAY
}

enum class TransactionStatus {
    OPEN, HOLD, IN_PROGRESS, SUCCESS, REFUND, ERROR, CANCEL
}
