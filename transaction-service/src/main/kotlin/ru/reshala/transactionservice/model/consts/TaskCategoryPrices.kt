package ru.reshala.transactionservice.model.consts

object CourierCategoryPrices{
    const val costPerKilometer: Double = 6.0
    const val costPerMinute: Double = 9.0
    const val minCost: Double = 80.0
}

object CleanerCategoryPrices{
    const val costPerSquareMeter: Double = 25.0
    const val costForWindowCleaning: Double = 1200.0
    const val minCost: Double = 300.0
}

object LoaderCategoryPrices{
    const val costMinute: Double = 26.0
    const val minCost: Double = 390.0
}

object GarbageCategoryPrices{
    const val costForGarbageBag: Double = 30.0
    const val minCost: Double = 90.0
}