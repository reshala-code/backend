package ru.reshala.transactionservice.controller

import io.swagger.annotations.ApiOperation
import org.springframework.web.bind.annotation.*
import ru.reshala.transactionservice.model.Account
import ru.reshala.transactionservice.service.AccountService
import ru.reshala.transactionservice.service.TransactionService
import java.util.*
import javax.persistence.Column
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Id

@RestController
@RequestMapping("/account")
@CrossOrigin
class AccountController(val accountService: AccountService) {
    @ApiOperation("Создать счет пользователя")
    @PostMapping("/create")
    fun createAccount(): Account = accountService.createAccount();

    @ApiOperation("Пополнить")
    @PostMapping("/deposit-emulation")
    fun createAccount(@RequestBody sum : Double) = accountService.emulateDeposit(sum);

    @ApiOperation("Получить счет текущего пользователя")
    @GetMapping()
    fun getAccount() = accountService.getAccount();

}
