package ru.reshala.transactionservice.controller

import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.springframework.web.bind.annotation.*
import ru.reshala.transactionservice.model.Task
import ru.reshala.transactionservice.model.TransactionType
import ru.reshala.transactionservice.service.TransactionService
import java.util.*

@RestController
@RequestMapping()
@CrossOrigin
class TransactionController(val transactionService: TransactionService) {

    @ApiOperation("Произвести расчет стоимости задачи")
    @PostMapping("/task/cost")
    fun getTaskCost(
        @ApiParam("Объект задачи")
        @RequestBody task: Task,
    ) = transactionService.countTaskCost(task)

    @ApiOperation("Открыть транзакцию")
    @PostMapping("/transaction/open")
    fun open(
        @ApiParam("Id пользователя или null, если транзакция от промежуточного счета")
        @RequestParam from: String? = null,
        @ApiParam("Id пользователя или null, если транзакция до промежуточного счета")
        @RequestParam to: String? = null,
        @ApiParam("Id задачи, с которой связана операция")
        @RequestParam entityId: String? = null,
        @ApiParam("Сумма транзакции")
        @RequestParam value: Double,
        @ApiParam("Тип транзакции")
        @RequestParam type: TransactionType,
    ) = transactionService.open(from, to, entityId, value, value, type)

    @ApiOperation("Начать исполнение транзакции",
        notes = "Если указан параметр transactionId, то entityId и type заполнять не обязательно." +
                "Если указаны entityId и type, то transactionId заполнять не обязательно")
    @PostMapping("/transaction/inProgress")
    fun inProgress(
        @ApiParam("ID транзакции. Необязательный параметр, если заполнены entityId и type" )
        @RequestParam transactionId: UUID? = null,
        @ApiParam("Id задачи, с которой связана операция. Обязательный параметр, если не указан transactionId")
        @RequestParam entityId: String? = null,
        @ApiParam("Тип транзакции. Обязательный параметр, если не указан transactionId")
        @RequestParam type: TransactionType? = null,
    ) = transactionService.inProgress(transactionId = transactionId, entityId = entityId, type = type)

    @ApiOperation("Отменить транзакцию",
        notes = "Если указан параметр transactionId, то entityId и type заполнять не обязательно." +
                "Если указаны entityId и type, то transactionId заполнять не обязательно")
    @PostMapping("/transaction/cancel")
    fun cancel(
        @ApiParam("ID транзакции. Необязательный параметр, если заполнены entityId и type" )
        @RequestParam transactionId: UUID? = null,
        @ApiParam("Id задачи, с которой связана операция. Обязательный параметр, если не указан transactionId")
        @RequestParam entityId: String? = null,
        @ApiParam("Тип транзакции. Обязательный параметр, если не указан transactionId")
        @RequestParam type: TransactionType? = null,
    ) = transactionService.cancel(transactionId = transactionId, entityId = entityId, type = type)

    @GetMapping("/transaction/user/{userId}")
    fun getUserTransactions(
        @ApiParam("ID пользователя")
        @PathVariable userId: String) = transactionService.getUserTransactions(userId);
}
