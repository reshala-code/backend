package ru.reshala.transactionservice.configuration

import feign.RequestInterceptor
import feign.RequestTemplate
import org.keycloak.KeycloakPrincipal
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken
import org.springframework.context.annotation.Bean
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient
import org.springframework.stereotype.Component


class CustomDefaultFeignConfiguration {

    @Bean
    fun getRequestInterceptor(): RequestInterceptor {
        return UserFeignClientInterceptor()
    }
}


class UserFeignClientInterceptor : RequestInterceptor {
    override fun apply(template: RequestTemplate) {
        val securityContext: SecurityContext = SecurityContextHolder.getContext()
        val authentication: Authentication? = securityContext.authentication
        if (authentication != null && authentication is KeycloakAuthenticationToken) {
            val tokenString = (authentication.principal as KeycloakPrincipal<*>).keycloakSecurityContext.tokenString
            template.header(AUTHORIZATION_HEADER,
                String.format("%s %s", BEARER_TOKEN_TYPE, tokenString))
        }
    }

    companion object {
        private const val AUTHORIZATION_HEADER = "Authorization"
        private const val BEARER_TOKEN_TYPE = "Bearer"
    }
}
