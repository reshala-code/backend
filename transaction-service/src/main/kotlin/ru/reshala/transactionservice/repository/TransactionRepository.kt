package ru.reshala.transactionservice.repository

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import ru.reshala.transactionservice.model.Transaction
import ru.reshala.transactionservice.model.TransactionType
import java.util.UUID

@Repository
interface TransactionRepository : PagingAndSortingRepository<Transaction,UUID>{
    fun findOneById(id: UUID): Transaction?
    fun findByEntityIdAndType(entityId: String, type: TransactionType): Transaction?
    fun findByFromOrTo(userId: String, userId1: String) : List<Transaction>
}
