package ru.reshala.transactionservice.repository

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import ru.reshala.transactionservice.model.Account
import ru.reshala.transactionservice.model.AccountType
import java.util.UUID

@Repository
interface AccountRepository : PagingAndSortingRepository<Account,UUID>{
    fun findOneByUserId(userId: UUID): Account?
    fun findByType(type: AccountType): Account?
}
