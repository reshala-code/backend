package ru.reshala.transactionservice

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.cloud.openfeign.EnableFeignClients

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
class TransactionApplication

fun main(args: Array<String>) {
    SpringApplication.run(TransactionApplication::class.java, *args)
}
