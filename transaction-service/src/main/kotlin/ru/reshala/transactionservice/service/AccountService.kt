package ru.reshala.transactionservice.service

import org.springframework.stereotype.Service
import ru.reshala.transactionservice.client.GeoClient
import ru.reshala.transactionservice.client.PromocodeClient
import ru.reshala.transactionservice.client.TaskClient
import ru.reshala.transactionservice.exception.NotFoundException
import ru.reshala.transactionservice.model.Account
import ru.reshala.transactionservice.model.TransactionType
import ru.reshala.transactionservice.repository.AccountRepository
import ru.reshala.transactionservice.repository.TransactionRepository
import ru.reshala.transactionservice.utils.CurrentUser

@Service
class AccountService(
    val accountRepository: AccountRepository,
    val transactionService: TransactionService,
) {
    fun createAccount(): Account {
        val account = Account(userId = CurrentUser.userIdAsUUID)
        return accountRepository.save(account)
    }

    fun emulateDeposit(sum: Double) {
        val userId = CurrentUser.userIdAsUUID
        val userAccount = accountRepository.findOneByUserId(userId)
            ?: throw NotFoundException("Не найден счет пользователя с таким id: ${userId}")
        transactionService.open(null, userAccount.userId.toString(), null, sum, sum, TransactionType.DEPOSIT)
    }

    fun getAccount(): Account? {
        val userId = CurrentUser.userIdAsUUID
        return accountRepository.findOneByUserId(userId)

    }
}
