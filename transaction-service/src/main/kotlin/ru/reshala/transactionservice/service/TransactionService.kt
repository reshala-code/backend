package ru.reshala.transactionservice.service

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import ru.reshala.transactionservice.client.GeoClient
import ru.reshala.transactionservice.client.HereMapsClient
import ru.reshala.transactionservice.client.HereMapsTransportMode
import ru.reshala.transactionservice.client.PromocodeClient
import ru.reshala.transactionservice.client.PromocodeUnit
import ru.reshala.transactionservice.client.TaskClient
import ru.reshala.transactionservice.client.UserPromocode
import ru.reshala.transactionservice.client.convertToWayPointsMap
import ru.reshala.transactionservice.client.getInHereMapsCoordinatesFormat
import ru.reshala.transactionservice.exception.BadRequestException
import ru.reshala.transactionservice.exception.NotFoundException
import ru.reshala.transactionservice.model.*
import ru.reshala.transactionservice.model.TaskCategoryName.*
import ru.reshala.transactionservice.model.TransactionStatus.*
import ru.reshala.transactionservice.model.TransactionType.*
import ru.reshala.transactionservice.model.consts.CleanerCategoryPrices
import ru.reshala.transactionservice.model.consts.CourierCategoryPrices
import ru.reshala.transactionservice.model.consts.GarbageCategoryPrices
import ru.reshala.transactionservice.model.consts.LoaderCategoryPrices
import ru.reshala.transactionservice.repository.AccountRepository
import ru.reshala.transactionservice.repository.TransactionRepository
import java.util.*

private const val reshalaCommission = 25.0

@Service
@Transactional
class TransactionService(
    val geoClient: GeoClient,
    val promocodeClient: PromocodeClient,
    val accountRepository: AccountRepository,
    val transactionRepository: TransactionRepository,
    val taskClient: TaskClient,
    val hereMapsClient: HereMapsClient,
    val objectMapper: ObjectMapper
) {

    private val reshalaAccount = accountRepository.findByType(AccountType.RESHALA_ACCOUNT)

    /**
     *
     * @param Объект задачи для которой надо посчитать стоимость
     * @return Стоимость выполнения задачи
     */
    fun countTaskCost(task: Task): TaskCost {
        // расчеты
        val cost = when (task.type) {
            COURIER -> {
                countCourierTaskCost(task.addresses)
            }

            CLEANER -> {
                val categoryInfo = objectMapper.convertValue(task.categoryInfo,
                    CleanerCategoryInfo::class.java)
                countCleanerTaskCost(categoryInfo)
            }

            LOADER -> {
                val categoryInfo = objectMapper.convertValue(task.categoryInfo,
                    LoaderCategoryInfo::class.java)
                countLoaderTaskCost(categoryInfo)
            }

            GARBAGE ->{
                val categoryInfo = objectMapper.convertValue(task.categoryInfo,
                    GarbageCategoryInfo::class.java)
                countGarbageTaskCost(categoryInfo)
            }

        }.toInt()
        //ask promocode service
        val finalCost = if (task.promocode != null) {
            val promocodeDetails = promocodeClient.usePromocode(task.promocode)
            countFinalCost(cost, promocodeDetails)
        } else {
            cost
        }
        // set values
        return TaskCost(cost, finalCost)
    }

    private fun countFinalCost(cost: Int, promocodeDetails: UserPromocode?): Int {
        val promocode = promocodeDetails!!.promocode
        val finalCost = when (promocode.unit) {
            PromocodeUnit.PERCENT -> {
                val pureDiscount = cost - cost / 100 * promocode.value
                val discount = if (pureDiscount > promocode.maxValue!!) promocode.maxValue.toDouble() else pureDiscount.toDouble()
                cost - discount
            }
            PromocodeUnit.RUB -> {
                cost - promocode.value
            }
        }
        return finalCost.toInt()
    }

    /**
     * чекает достаточно ли средств у пользователя для исполненеия заказа
     * чекает баланс и делает всякие движухи
     * если денег
     */
    fun isEnoughMoneyForTask(task: Task): Boolean {
        return true
    }

    private fun countTaskCostInternal(task: Task): Double {
        geoClient.getDistanceAndDuration(task.addresses)
        val maxCost = 22
        return maxCost.toDouble()
//        return when (status) {
//            TaskStatus.FINISHED -> finish(task)
//            TaskStatus.CANCELLED -> cancel(task)
//            TaskStatus.IN_PROGRESS -> start(task)
//            TaskStatus.CREATED -> returnToFindingWorkerQueue(task)
//            TaskStatus.NOT_STARTED_INSUFFICIENT_FUNDS -> changeTaskStatus(taskId,
//                TaskStatus.NOT_STARTED_INSUFFICIENT_FUNDS)
//            TaskStatus.IMPLEMENTER_NOT_FOUND -> changeTaskStatus(taskId, TaskStatus.IMPLEMENTER_NOT_FOUND)
//        }
    }

    fun open(
        from: String? = null,
        to: String? = null,
        entityId: String? = null,
        value: Double,
        finalValue: Double = value,
        type: TransactionType,
    ) {
        when (type) {
            TASK_PAY -> {
                if (from.isNullOrBlank() || entityId.isNullOrBlank()) {
                    throw BadRequestException("Некорректные параметры запроса для транзакции с типом $type")
                }

                val transaction = Transaction(from = from,
                    to = reshalaAccount?.userId.toString(),
                    entityId = entityId,
                    value = value,
                    finalValue = value,
                    type = type)
                transactionRepository.save(transaction).also { hold(it.id, value, finalValue) }
            }

            WORKER_PAY -> {
                if (to.isNullOrBlank() || entityId.isNullOrBlank()) {
                    throw BadRequestException("Некорректные параметры запроса для транзакции с типом $type")
                }

                val transaction = Transaction(from = reshalaAccount?.userId.toString(),
                    to = to,
                    value = value,
                    commission = reshalaCommission,
                    finalValue = finalValue,
                    type = type)
                transactionRepository.save(transaction).also { inProgress(transaction.id) }
            }

            DEPOSIT -> {
                if (to.isNullOrBlank()) {
                    throw BadRequestException("Некорректные параметры запроса для транзакции с типом $type")
                }

                val transaction = Transaction(
                    from = null,
                    to = to,
                    value = value,
                    finalValue = finalValue,
                    type = type)
                transactionRepository.save(transaction).also { inProgress(transaction.id) }
            }

            else -> throw BadRequestException("Некорректные параметры запроса для транзакции с типом $type")
        }
    }

    fun inProgress(transactionId: UUID? = null, entityId: String? = null, type: TransactionType? = null) {
        val transaction = getTransaction(transactionId, entityId, type)

        val toAccount = accountRepository.findOneByUserId(UUID.fromString(transaction.to))
            ?: throw  NotFoundException("Не найден счет для пользователя с id ${transaction.to}")

        toAccount.balance = toAccount.balance + transaction.value
        transaction.status = IN_PROGRESS

        transactionRepository.save(transaction).also { end(transactionId, entityId, type) }
    }

    fun end(transactionId: UUID? = null, entityId: String? = null, type: TransactionType? = null) {
        val transaction = getTransaction(transactionId, entityId, type)

        transaction.status = SUCCESS
        transactionRepository.save(transaction)

        if (transaction.entityId != null) {
            if (transaction.type == TASK_PAY) {
                val task = taskClient.getById(transaction.entityId.toLong())
                val valueMinusCommission = transaction.value - transaction.value / 100.0 * reshalaCommission

                open(reshalaAccount?.id.toString(),
                    task.implementerId!!.toString(),
                    transaction.entityId,
                    transaction.value,
                    valueMinusCommission,
                    WORKER_PAY)
            }
        }
    }

    fun cancel(
        transactionId: UUID? = null,
        entityId: String? = null,
        type: TransactionType? = null,
    ): Boolean {
        val transaction = getTransaction(transactionId, entityId, type)
        when (transaction.status) {
            OPEN -> {
                transaction.status = CANCEL
            }
            HOLD -> {
                val account = accountRepository.findOneByUserId(UUID.fromString(transaction.from))
                    ?: throw NotFoundException("Не найден счет для пользователя с id ${transaction.from}")
                account.balance = account.balance - transaction.finalValue
                accountRepository.save(account)
                transaction.status = CANCEL
                transactionRepository.save(transaction)
            }
            else -> throw BadRequestException("Невозможно отменить операцию находящуюся в статусе ${transaction.status}")
        }
        return true


    }

    private fun hold(transactionId: UUID, value: Double, finalValue: Double) {
        val transaction = getTransaction(transactionId)

        val fromAccount = accountRepository.findOneByUserId(UUID.fromString(transaction.from))
            ?: throw NotFoundException("Не найден счет для пользователя с id ${transaction.from}")

        if (fromAccount.balance < value) {
            transaction.status = ERROR
            transactionRepository.save(transaction)
            throw BadRequestException("Недостаточно средств на счету")
        }

        fromAccount.balance = fromAccount.balance - value
        transaction.status = HOLD

        transactionRepository.save(transaction)
    }

    fun getUserTransactions(userId: String) = transactionRepository.findByFromOrTo(userId, userId)

    private fun getTransaction(
        transactionId: UUID? = null,
        entityId: String? = null,
        type: TransactionType? = null,
    ): Transaction {
        if (transactionId == null && (entityId.isNullOrBlank() || type == null)) {
            throw BadRequestException("Некорреткные параметры для поиска транзакции")
        }

        return if (transactionId != null) {
            transactionRepository.findOneById(transactionId)
                ?: throw NotFoundException("Транзакция с таким id не найдена")
        } else {
            transactionRepository.findByEntityIdAndType(entityId!!, type!!)
                ?: throw NotFoundException("Транзакция с таким entityId и type не найдена")
        }
    }

    private fun countCourierTaskCost(addresses: List<Address>): Double{
        val wayPoints = convertToWayPointsMap(
                listOf(
                        getInHereMapsCoordinatesFormat(addresses[0].latitude, addresses[0].longitude),
                        getInHereMapsCoordinatesFormat(addresses[1].latitude, addresses[1].longitude)
                )
        )

        val busRoutes = hereMapsClient.findRoute(wayPoints = wayPoints).routes

        val pedestrianRoutes = hereMapsClient.findRoute(
                wayPoints = wayPoints,
                mode = HereMapsTransportMode.PEDESTRIAN_WITHOUT_TRAFFIC.value
        ).routes

        val minRoute =  if (busRoutes.isEmpty() && pedestrianRoutes.isEmpty())
            throw NotFoundException("Невозможно построить маршрут между указанными адресами или попробуйте позже")
        else
            if (busRoutes[0].summary.travelTime.compareTo(
                            pedestrianRoutes[0].summary.travelTime
                    ) == -1
            ) busRoutes[0]
            else pedestrianRoutes[0]

        val cost = minRoute.summary.travelTime/60.0 * CourierCategoryPrices.costPerMinute + minRoute.summary.distance!!/1000.0 * CourierCategoryPrices.costPerKilometer

        return if (cost< CourierCategoryPrices.minCost) CourierCategoryPrices.minCost else cost

    }

    private fun countCleanerTaskCost(categoryInfo: CleanerCategoryInfo): Double {
        var cost = categoryInfo.squareMeters * CleanerCategoryPrices.costPerSquareMeter

        if (categoryInfo.isCleaningWindowOn){
            cost += CleanerCategoryPrices.costForWindowCleaning
        }

        return if (cost< CleanerCategoryPrices.minCost) CleanerCategoryPrices.minCost else cost
    }

    private fun countLoaderTaskCost(categoryInfo: LoaderCategoryInfo): Double{
        val cost = categoryInfo.minutes * LoaderCategoryPrices.costMinute

        return if (cost< LoaderCategoryPrices.minCost) LoaderCategoryPrices.minCost else cost
    }

    private fun countGarbageTaskCost(categoryInfo: GarbageCategoryInfo): Double{
        val cost = categoryInfo.bagCount * GarbageCategoryPrices.costForGarbageBag

        return if (cost< GarbageCategoryPrices.minCost) GarbageCategoryPrices.minCost else cost
    }

}




