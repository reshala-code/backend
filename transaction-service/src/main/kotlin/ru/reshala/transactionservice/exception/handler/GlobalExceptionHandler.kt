package ru.reshala.transactionservice.exception.handler

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import ru.reshala.transactionservice.exception.BadRequestException
import ru.reshala.transactionservice.exception.ForbiddenException
import ru.reshala.transactionservice.exception.NotFoundException
import ru.reshala.transactionservice.exception.ServerException
import ru.reshala.transactionservice.exception.ServiceError

@ControllerAdvice
class GlobalExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(BadRequestException::class)
    protected fun handleBadRequestException(exception: BadRequestException, request: WebRequest) =
        handleException(exception, HttpStatus.BAD_REQUEST, request)

    @ExceptionHandler(ForbiddenException::class)
    protected fun handleForbiddenException(exception: ForbiddenException, request: WebRequest) =
        handleException(exception, HttpStatus.FORBIDDEN, request)

    @ExceptionHandler(NotFoundException::class)
    protected fun handleNotFoundException(exception: NotFoundException, request: WebRequest) =
        handleException(exception, HttpStatus.NOT_FOUND, request)

    @ExceptionHandler(ServerException::class)
    protected fun handleServerException(exception: ServerException, request: WebRequest) =
        handleException(exception, HttpStatus.INTERNAL_SERVER_ERROR, request)

    private fun handleException(exception: Exception, status: HttpStatus, request: WebRequest) =
        handleExceptionInternal(
            exception,
            ServiceError(code = status.value(), message = exception.message),
            HttpHeaders(),
            status,
            request
        )

    override fun handleHttpMessageNotReadable(
        ex: HttpMessageNotReadableException,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any> {
        return handleException(ex, HttpStatus.BAD_REQUEST, request)
    }

}
