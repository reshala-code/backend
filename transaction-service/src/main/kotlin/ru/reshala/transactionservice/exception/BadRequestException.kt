package ru.reshala.transactionservice.exception

class BadRequestException : TransactionServiceException {
    constructor(message: String) : super(message)
    constructor(message: String, exception: Exception) : super(message, exception)
}
