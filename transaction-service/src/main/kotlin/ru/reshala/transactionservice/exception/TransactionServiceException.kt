package ru.reshala.transactionservice.exception

abstract class TransactionServiceException: RuntimeException {
    constructor(message: String): super(message)
    constructor(message: String, exception: Exception): super(message, exception)
}
