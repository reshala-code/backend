package ru.reshala.transactionservice.exception

class ServerException: TransactionServiceException {
    constructor(message: String): super(message)
    constructor(message: String, exception: Exception): super(message, exception)
}
