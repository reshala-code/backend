package ru.reshala.transactionservice.exception

class NotFoundException: TransactionServiceException {
    constructor(message: String): super(message)
    constructor(message: String, exception: Exception): super(message, exception)
}
