package ru.reshala.transactionservice.exception

import  java.io.Serializable

data class ServiceError (
    val code: Int,
    val message: String?
): Serializable {
    override fun hashCode(): Int {
        return super.hashCode()
    }

    override fun toString(): String {
        return super.toString()
    }
}
