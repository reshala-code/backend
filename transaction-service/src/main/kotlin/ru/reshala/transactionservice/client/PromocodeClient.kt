package ru.reshala.transactionservice.client

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import ru.reshala.transactionservice.configuration.CustomDefaultFeignConfiguration
import java.util.*

@Component
@FeignClient(name = "promocode-service", configuration = [CustomDefaultFeignConfiguration::class])
interface PromocodeClient {
    @PostMapping("user-promocode/using")
    fun usePromocode(
        @RequestBody code: String?
    ): UserPromocode?
}

class UserPromocode (
    val userId: UUID,
    val promocode: Promocode,
    val usagesLeft: Int?,
    val isActivated: Boolean = false,
)

data class Promocode(
    val unit: PromocodeUnit,
    val value: Int,
    val maxValue: Int? = null,
    val usages: Int? = null,
)

enum class PromocodeUnit {
    PERCENT, RUB
}
