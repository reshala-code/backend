package ru.reshala.transactionservice.client

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import ru.reshala.transactionservice.configuration.CustomDefaultFeignConfiguration
import ru.reshala.transactionservice.model.Task

@Component
@FeignClient(name = "task-service",configuration = [CustomDefaultFeignConfiguration::class])
interface TaskClient {
    @GetMapping("/task/{taskId}")
    fun getById(
        @PathVariable taskId: Long
    ): Task
}
