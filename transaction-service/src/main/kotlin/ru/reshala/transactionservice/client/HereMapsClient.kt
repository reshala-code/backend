package ru.reshala.transactionservice.client

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import ru.reshala.transactionservice.configuration.HereMapsFeignClientConfiguration


@FeignClient(name = "here-maps-service",
    url = "https://route.ls.hereapi.com/routing/7.2")
interface HereMapsClient {
    @GetMapping("/calculateroute.json")
    fun findRoute(
        @RequestParam apiKey: String = "QsF7N_L7ULKt6SrzyGXMR1rfqcavoUp6I5XiU4L8K80",
        @RequestParam wayPoints: Map<String, String>,
        @RequestParam mode: String = HereMapsTransportMode.PUBLIC_TRANSPORT_WITH_TRAFFIC.value,
        @RequestParam alternatives: Int = 0,
        @RequestParam jsonAttributes: Int = 153,
        @RequestParam language: String = "ru-RU",
        @RequestParam("return") whatReturn: String = "summary",
    ): RouteInfoResponse
}

enum class HereMapsTransportMode(val value: String) {
    BICYCLE_WITHOUT_TRAFFIC("fastest;bicycle;traffic:disabled"),
    CAR_WITH_TRAFFIC("fastest;car;traffic:enabled"),
    PUBLIC_TRANSPORT_WITH_TRAFFIC("fastest;publicTransport;traffic:enabled"),
    PEDESTRIAN_WITHOUT_TRAFFIC("fastest;pedestrian;traffic:disabled");
}

data class RouteInfoResponse(val routes: List<Route> = emptyList()) {
    data class Route(
        val summary: Summary,
    ) {
        data class Summary(
            val distance: Long? = null,
            val baseTime: Long? = null,
            val flags: List<String> = emptyList(),
            val text: String? = null,
            val travelTime: Long = Long.MAX_VALUE,
            val departure: String? = null,
            @JsonProperty("_type")
            val type: String? = null,
        )
    }
}

fun RouteInfoResponse.Route?.getDuration() = this?.summary?.travelTime

fun getInHereMapsCoordinatesFormat(latitude: Double, longitude: Double) = "geo!$latitude,$longitude"

fun convertToWayPointsMap(wayPoints: List<String>) =
    wayPoints.mapIndexed { index, coordinate -> "waypoint$index" to coordinate }.toMap()
