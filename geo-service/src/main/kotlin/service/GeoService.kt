package ru.reshala.geoservice.service

import org.springframework.data.mongodb.core.geo.GeoJsonPoint
import org.springframework.stereotype.Service
import ru.reshala.geoservice.client.Notification
import ru.reshala.geoservice.client.NotificationClient
import ru.reshala.geoservice.client.NotificationType
import ru.reshala.geoservice.client.TaskClient
import ru.reshala.geoservice.model.Position
import ru.reshala.geoservice.model.User
import ru.reshala.geoservice.repository.GeoRepository
import ru.reshala.geoservice.utils.CurrentUser
import java.util.*

@Service
class GeoService(
    val geoRepository: GeoRepository,
    val taskClient: TaskClient,
    val notificationClient: NotificationClient,
) {

    fun updateUserPlace(userId: String, longitude: Double, latitude: Double) {
        geoRepository.upsert(User(userId = userId,
            location = GeoJsonPoint(longitude, latitude),
            id = null)
        )
        val task = taskClient.getCurrentTaskByUserId(userId)
        if (task != null) {
            val creatorId = task.creatorId;
            val notification = Notification(creatorId.toString(),
                type = NotificationType.WORKER_POSITION_CHANGED,
                Position(latitude = latitude, longitude = longitude))
            notificationClient.sendNotification(notification)
            println("send notification")
            println(latitude.toString() + " " + longitude)

        }
    }

    fun findByUserId(userId: UUID?): User? {
        return geoRepository.findByUserId(userId.toString())
    }
}
