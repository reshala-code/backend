package ru.reshala.geoservice.service

import org.springframework.data.mongodb.core.mapping.event.AfterSaveEvent

import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener
import org.springframework.stereotype.Component
import ru.reshala.geoservice.model.User


@Component
class MongoListener : AbstractMongoEventListener<User?>() {

    override fun onAfterSave(event: AfterSaveEvent<User?>) {
        super.onAfterSave(event)
        val source = event.source
        println("onaftersave")
        println(source)

    }
}
