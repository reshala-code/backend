package ru.reshala.geoservice.client

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import ru.reshala.geoservice.model.Task

@Component
@FeignClient(name = "task-service")
interface TaskClient {
    @GetMapping("/task/active-task/{userId}")
    fun getCurrentTaskByUserId(@PathVariable userId:String): Task?
}
