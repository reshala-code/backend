package ru.reshala.geoservice.model

data class Position(
    val latitude: Double,
    val longitude: Double,
)
