package ru.reshala.geoservice.model

import com.mongodb.client.model.geojson.Point
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.geo.GeoJsonPoint
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.MongoId
import java.util.*


@Document(collection = "users")
data class User(
    @MongoId
    val id: ObjectId?,
    @Indexed
    val userId: String?,
    @GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
    var location: GeoJsonPoint
)

fun User.toGeoUserResponse() = GeoUserResponse(userId = UUID.fromString(this.userId), latitude = this.location.y,longitude = this.location.x)

data class GeoUserResponse(
    val userId: UUID?,
    val latitude: Double?,
    val longitude: Double?
)
