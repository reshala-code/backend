package ru.reshala.geoservice.model

import java.util.*

data class UserPlaceRequest (
    val userId: UUID?,
    val longitude:Double,
    val latitude: Double
)

