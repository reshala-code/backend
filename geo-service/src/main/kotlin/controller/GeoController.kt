package ru.reshala.geoservice.controller

import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.springframework.data.geo.Distance
import org.springframework.data.geo.Metrics
import org.springframework.data.geo.Point
import org.springframework.data.mongodb.core.geo.GeoJsonPoint
import org.springframework.web.bind.annotation.*
import ru.reshala.geoservice.client.TaskClient
import ru.reshala.geoservice.model.*
import ru.reshala.geoservice.repository.GeoRepository
import ru.reshala.geoservice.service.GeoService
import ru.reshala.geoservice.utils.CurrentUser
import java.util.*

@RestController
@RequestMapping("/geo")
@CrossOrigin
class GeoController(
    val geoRepository: GeoRepository,
    val geoService: GeoService,
    val taskClient: TaskClient,
) {
    @ApiOperation("Поиск исполнителей по заданным координатам и радиусу ")
    @GetMapping
    fun getSuitableUser(
        @ApiParam("Широта") @RequestParam latitude: Double,
        @ApiParam("Долгота") @RequestParam longitude: Double,
        @ApiParam("Радиус") @RequestParam distance: Double = 5.0,
    ): MutableList<GeoUserResponse> {
        val users = geoRepository.findByLocationNear(Point(longitude, latitude), Distance(distance, Metrics.KILOMETERS))
        return users.map(User::toGeoUserResponse).toMutableList()
    }

    @ApiOperation("Создает запись о местоположении пользователя")
    @PostMapping
    fun saveUserGeo(
        @ApiParam("Объект описывающий местоположение пользователя") @RequestBody userPlaceRequest: UserPlaceRequest,
    ) {
        val user = User(userId = userPlaceRequest.userId.toString(),
            location = GeoJsonPoint(userPlaceRequest.longitude, userPlaceRequest.latitude),
            id = null)
        geoRepository.save(user)
    }

    @ApiOperation("Создает или обновляет запись о местоположении пользователя")
    @PutMapping
    fun updateUserPlace(
        @ApiParam("Объект описывающий местоположение пользователя")
        @RequestBody userPlaceRequest: UserPlaceRequest,
    ): GeoUserResponse {
        geoService.updateUserPlace(CurrentUser.userId, userPlaceRequest.longitude, userPlaceRequest.latitude)
        return GeoUserResponse(UUID.fromString(CurrentUser.userId),
            userPlaceRequest.latitude,
            userPlaceRequest.longitude)
    }

    @GetMapping("/task")
    fun getWorkerPosition(): Position? {
        val task = taskClient.getCurrentTaskByUserId(CurrentUser.userId)
        val userPosition = geoService.findByUserId(task?.implementerId)?.toGeoUserResponse()
        if (userPosition == null) return null;
        return Position(userPosition.latitude!!, userPosition.longitude!!)
    }
}


