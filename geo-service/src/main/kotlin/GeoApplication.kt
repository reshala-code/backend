package ru.reshala.geoservice

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories

@SpringBootApplication
//@EnableMongoRepositories
@EnableEurekaClient
@EnableFeignClients
class GeoApplication

fun main(args: Array<String>) {
    SpringApplication.run(GeoApplication::class.java, *args)
}
