package ru.reshala.geoservice.configuration

import feign.RequestInterceptor
import feign.RequestTemplate
import org.keycloak.KeycloakPrincipal
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import java.lang.String

@Component
class UserFeignClientInterceptor : RequestInterceptor {
    override fun apply(template: RequestTemplate) {
        val securityContext: SecurityContext = SecurityContextHolder.getContext()
        val authentication: Authentication? = securityContext.authentication
        if (authentication != null && authentication is KeycloakAuthenticationToken) {
            template.header(AUTHORIZATION_HEADER,
                String.format("%s %s", BEARER_TOKEN_TYPE, (authentication.principal as KeycloakPrincipal<*>).keycloakSecurityContext.tokenString))
        }
    }

    companion object {
        private const val AUTHORIZATION_HEADER = "Authorization"
        private const val BEARER_TOKEN_TYPE = "Bearer"
    }
}
