package ru.reshala.geoservice.repository

import org.springframework.data.mongodb.core.FluentMongoOperations
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.upsert
import ru.reshala.geoservice.model.User
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Update
import org.springframework.transaction.annotation.Transactional

class RepositoryCustomImpl(val mongoTemplate: MongoTemplate) : RepositoryCustom {
    @Transactional
    override fun upsert(user: User){
        val query = Query()
        query.addCriteria(Criteria.where("userId").`is`(user.userId))
        val update = Update()
        update.set("location", user.location)
        mongoTemplate.upsert(query,update,User::class.java)
    }
}
