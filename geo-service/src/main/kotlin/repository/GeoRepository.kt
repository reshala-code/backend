package ru.reshala.geoservice.repository

import org.springframework.data.geo.Distance
import org.springframework.data.geo.Point
import org.springframework.data.mongodb.core.geo.GeoJsonPoint
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query
import org.springframework.stereotype.Repository
import ru.reshala.geoservice.model.User

@Repository
interface GeoRepository: MongoRepository<User, String>, RepositoryCustom {

    fun findByLocationNear(location:Point, distance:Distance): List<User>
    fun findByUserId(userId:String): User?

}
