package ru.reshala.geoservice.repository

import org.springframework.stereotype.Repository
import ru.reshala.geoservice.model.User

@Repository
interface RepositoryCustom {
    fun upsert(user: User)
}
