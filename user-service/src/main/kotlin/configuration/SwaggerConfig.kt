package ru.reshala.userservice.configuration

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Info
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.ArrayList

import springfox.documentation.service.ApiInfo

import springfox.documentation.builders.PathSelectors

import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.Contact

import springfox.documentation.spi.DocumentationType

import springfox.documentation.spring.web.plugins.Docket

import springfox.documentation.swagger2.annotations.EnableSwagger2
import springfox.documentation.builders.ApiInfoBuilder





// Enabling Swagger Version 2
@Configuration // Add spring configuration behaviour to the config class
@OpenAPIDefinition(info =
    Info(title = "User-service", version = "1.0", description = "Описание API user-service")
)
class SwaggerConfig {
    @Bean
    fun api(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
            .select() //Here adding base package to scan controllers. This will scan only controllers inside
            //specific package and include in the swagger documentation
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.any())
            .build()
    }
}
