package ru.reshala.userservice.keycloak

import org.keycloak.admin.client.Keycloak
import org.keycloak.representations.idm.RoleRepresentation
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import ru.reshala.userservice.utlis.CurrentUser

@Service
class KeycloakAdminClient(
    private val keycloak: Keycloak,
    @Value("\${keycloak.realm}")
    private val realm: String,
    @Value("\${keycloak.resource}")
    private val clientId: String,

    ) {
    val clientUUID: String

    init {
        clientUUID = keycloak.realm(realm).clients().findByClientId(clientId).get(0).id
    }


    fun assignRole(userId: String, vararg roleNames: String) {
        val roleRepresentations =
            roleNames.map { getRoleRepresentation(it) }

        keycloak
            .realm(realm)
            .users()
            .get(userId)
            .roles()
            .clientLevel(clientUUID)
            .add(roleRepresentations)
    }

    fun removeRole(userId: String, vararg roleNames: String) {
        val roleRepresentations =
            roleNames.map { getRoleRepresentation(it) }
        keycloak
            .realm(realm)
            .users()
            .get(userId)
            .roles()
            .clientLevel(clientUUID)
            .remove(roleRepresentations)
    }

    fun getRoleRepresentation(roleString: String): RoleRepresentation {

        return keycloak.realm(realm).clients().get(clientUUID).roles().get(roleString).toRepresentation()
    }

}
