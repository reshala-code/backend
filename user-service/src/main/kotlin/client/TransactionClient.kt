package ru.reshala.userservice.client

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import java.util.*

@Component
@FeignClient(name = "transaction-service")
interface TransactionClient {
    @PostMapping("/account/create")
    fun createAccount(

    ): Account

    @GetMapping("/account")
    fun findAccount(): Account?
}

data class Account(
    val id: UUID? = null,
    val userId: UUID? = null,
    var balance: Double = 0.0,
    val type: AccountType = AccountType.USER_ACCOUNT,
)

enum class AccountType {
    USER_ACCOUNT, COMPANY_ACCOUNT, RESHALA_ACCOUNT
}
