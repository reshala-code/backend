package ru.reshala.userservice.client

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import java.util.*

@Component
@FeignClient(name = "geo-service")
interface GeoClient {
    @PostMapping("/geo")
    fun saveUser(@RequestBody userPlaceRequest: UserPlaceRequest)
}

data class UserPlaceRequest (
    val userId:UUID,
    val longitude:Double?,
    val latitude: Double?
)
