package ru.reshala.userservice.service.interfaces

import ru.reshala.userservice.model.User
import ru.reshala.userservice.model.Worker
import java.util.*

interface UserService {
    fun getAllUsers(): List<User>?
    fun save(user: User): User
    fun getUser(userId: UUID): User
    fun getWorkers(status: String? = null): List<Worker>
    fun setStatus(workerId: UUID, status: String): Worker?
    fun getUsers(users: List<String>): Map<UUID, User>
    fun updateWorkerRating(workerId: UUID, mark: Byte, countOfWorkerTasks: Int)
}
