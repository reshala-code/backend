package ru.reshala.userservice.service.impl

import org.springframework.stereotype.Service
import ru.reshala.userservice.client.GeoClient
import ru.reshala.userservice.client.TransactionClient
import ru.reshala.userservice.client.UserPlaceRequest
import ru.reshala.userservice.exception.BadRequestException
import ru.reshala.userservice.exception.NotFoundException
import ru.reshala.userservice.keycloak.KeycloakAdminClient
import ru.reshala.userservice.model.Customer
import ru.reshala.userservice.model.User
import ru.reshala.userservice.model.Worker
import ru.reshala.userservice.repository.UserRepository
import ru.reshala.userservice.repository.WorkerRepository
import ru.reshala.userservice.service.interfaces.UserService
import ru.reshala.userservice.utlis.CurrentUser
import java.util.*

@Service
class UserServiceImpl(
    val geoClient: GeoClient,
    val transactionClient: TransactionClient,
    val userRepository: UserRepository,
    val workerRepository: WorkerRepository,
    val keycloakAdminClient: KeycloakAdminClient,
) : UserService {
    override fun getAllUsers(): List<User> = userRepository.findAll()

    override fun save(user: User): User {
        val savedUser = userRepository.save(user)
        if (transactionClient.findAccount() == null) {
            transactionClient.createAccount()
        }
        when (user) {
            is Worker -> {
                keycloakAdminClient.assignRole(CurrentUser.userId, "WORKER")
                keycloakAdminClient.removeRole(CurrentUser.userId, "PARTIALLY_REGISTERED")
            }
            is Customer -> {
                keycloakAdminClient.assignRole(CurrentUser.userId, "CUSTOMER")
                keycloakAdminClient.removeRole(CurrentUser.userId, "PARTIALLY_REGISTERED")
            }
        }
        return savedUser
    }


    override fun getUser(userId: UUID): User =
        userRepository.findOneById(userId) ?: throw BadRequestException("Not found any user with specified Id")

    override fun getWorkers(status: String?) =
        if (status.isNullOrBlank())
            workerRepository.findAll()
        else
            workerRepository.findAllByStatus(status)

    override fun setStatus(workerId: UUID, status: String): Worker? {
        val worker = workerRepository.findOneById(workerId)
        worker?.status = status
        return if (worker != null) workerRepository.save(worker) else null
    }

    override fun getUsers(users: List<String>): Map<UUID, User> {
        val usersAsUUID = users.map { UUID.fromString(it) }
        val findAllById = userRepository.findAllById(usersAsUUID)
        return findAllById.map {
            it.id to it
        }.toMap()
    }

    override fun updateWorkerRating(workerId: UUID, mark: Byte, countOfWorkerTasks: Int) {
        val worker = workerRepository.findOneById(workerId)?:
            throw NotFoundException("Not found any user with specified Id")

        worker.rating = (worker.rating!! * (countOfWorkerTasks-1) + mark)/countOfWorkerTasks

        workerRepository.save(worker)
    }
}
