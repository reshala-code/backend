package ru.reshala.userservice.model

import com.sun.org.apache.xpath.internal.operations.Bool
import org.hibernate.annotations.CreationTimestamp
import ru.reshala.userservice.utlis.CurrentUser
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*
import javax.validation.constraints.Email

@Entity
@Table(name = "usr")
abstract class User(
    @Id
    open var id: UUID, //= UUID.fromString(CurrentUser.userId),
    @Column(unique = true)
    open var username: String? = CurrentUser.userName,
    @Email
    @Column(unique = true)
    open var email: String? = CurrentUser.token.email,

    open var firstName: String? = CurrentUser.token.givenName,

    open var lastName: String? = CurrentUser.token.familyName,

    @Column(name = "email_verified")
    open var isEmailVerified: Boolean? = CurrentUser.token.emailVerified,
    @Column(unique = true)
    open var phoneNumber: String? = null,

    @Column(name = "phone_number_verified")
    open var isPhoneNumberVerified: Boolean = false,

//  open var registeredAt: LocalDateTime? = CurrentUser.token.

    @CreationTimestamp
    open var verifiedAt: LocalDateTime? = null

)
