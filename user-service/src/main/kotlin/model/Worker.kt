package ru.reshala.userservice.model

import org.springframework.core.convert.converter.Converter
import ru.reshala.userservice.utlis.CurrentUser
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

@Entity
class Worker(
    @Id
    override var id: UUID = UUID.fromString(CurrentUser.userId),
    @Column(unique = true)
    override var username: String? = CurrentUser.userName,
    @Column(columnDefinition = "text")
    var photo: String?,
    var status: String? = "Free",
    var preferredCategories: String?=null,
    var rating: Double? = 5.0,
    var patronymic: String?,
    var passportVerified: Boolean? = false
) : User(
    id,
    username,
    verifiedAt = LocalDateTime.now())
