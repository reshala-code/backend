package ru.reshala.userservice.model

import ru.reshala.userservice.utlis.CurrentUser
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*


@Entity
class Customer(
    @Id
    override var id: UUID = UUID.fromString(CurrentUser.userId),
    @Column(unique = true)
    override var username: String? = CurrentUser.userName
) : User(
    id,
    username,
    CurrentUser.userEmail,
    CurrentUser.token.givenName,
    CurrentUser.token.familyName,
    CurrentUser.token.emailVerified
)
