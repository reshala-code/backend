package model.dto;

import ru.reshala.userservice.model.User;

data class GetUserResponse(val user: User, val type: String?)
