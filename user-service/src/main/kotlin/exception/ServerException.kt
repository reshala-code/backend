package ru.reshala.userservice.exception

class ServerException: TaskServiceException {
    constructor(message: String): super(message)
    constructor(message: String, exception: Exception): super(message, exception)
}
