package ru.reshala.userservice.exception

class NotFoundException: TaskServiceException {
    constructor(message: String): super(message)
    constructor(message: String, exception: Exception): super(message, exception)
}
