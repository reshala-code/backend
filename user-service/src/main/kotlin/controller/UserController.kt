package ru.reshala.userservice.controller

import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import model.dto.GetUserResponse
import org.springframework.web.bind.annotation.*
import ru.reshala.userservice.exception.BadRequestException
import ru.reshala.userservice.model.Customer
import ru.reshala.userservice.model.Worker
import ru.reshala.userservice.service.interfaces.UserService

import javax.servlet.http.HttpServletRequest

import org.springframework.beans.factory.annotation.Autowired
import ru.reshala.userservice.model.User
import ru.reshala.userservice.utlis.CurrentUser
import java.util.*


@RestController
@RequestMapping("/user")
@CrossOrigin
class UserController(val userService: UserService) {
    @Autowired
    lateinit var request: HttpServletRequest

    @ApiOperation("Получить информацию о пользователе")
    @GetMapping("/{userId}")
    fun getUser(
        @ApiParam("ID пользователя")
        @PathVariable userId: UUID,
    ): GetUserResponse {
        val user = userService.getUser(userId)
        return when (user) {
            is Worker -> GetUserResponse(user, "Worker")
            is Customer -> GetUserResponse(user, "Customer")
            else -> throw BadRequestException("Can not find user with specified ID")
        }
    }

    @ApiOperation("Получить информацию об авторизованном пользователе")
    @GetMapping()
    fun getUser(): User = userService.getUser(UUID.fromString(CurrentUser.userId))

    @ApiOperation("Получить список исполнителей")
    @GetMapping("/worker", produces = ["application/json"])
    fun getWorkers(
        @ApiParam("Статус исполнителя. Если не указан, то вернет всех исполниетелй")
        @RequestParam status: String? = null,
    ): List<Worker> {
        println(request)
        return userService.getWorkers(status)
    }

    @ApiOperation("Получить информацию о пользователях по Id")
    @PostMapping("/users")
    fun getUsers(
        @ApiParam("Список id пользователей")
        @RequestBody users: List<String>
    ):Map<UUID, User>{
        return userService.getUsers(users)
    }

    @ApiOperation("Обновить информацию об исполнителе")
    @PutMapping("/worker")
    fun updateWorker(
        @ApiParam("Обновленный объект исполнителя")
        @RequestBody user: Worker,
    ) = userService.save(user)

    @ApiOperation("Обновить информацию об клиенте")
    @PutMapping("/customer")
    fun updateCustomer(
        @ApiParam("Обновленный объект клиента")
        @RequestBody user: Customer,
    ) = userService.save(user)

    @ApiOperation("Получить информацию об исполнителе по ID")
    @GetMapping("/worker/{workerId}")
    fun getWorker(
        @ApiParam("ID исполнителя")
        @PathVariable workerId: UUID,
    ) = userService.getUser(workerId)

    @ApiOperation("Получить информацию об клиенте по ID")
    @GetMapping("/customer/{customerId}")
    fun getCustomer(@PathVariable customerId: UUID) = userService.getUser(customerId)

    @ApiOperation("Зарегистрировать исполнителя (2-ой этап)")
    @PostMapping("/worker")
    fun createWorker(
        @ApiParam("Объект исполнителя")
        @RequestBody worker: Worker,
    ) = userService.save(worker)

    @ApiOperation("Зарегистрировать клиента (2-ой этап)")
    @PostMapping("/customer")
    fun createCustomer(
        @ApiParam("Объект клиента")
        @RequestBody user: Customer,
    ) = userService.save(user)

    @ApiOperation("Изменить статус исполнителя")
    @PutMapping("/{workerId}/status")
    fun changeWorkerStatus(
        @ApiParam("ID исполнителя")
        @PathVariable workerId: UUID,
        @ApiParam("Статус исполнителя")
        @RequestParam status: String,
    ) =
        userService.setStatus(workerId, status)

    @ApiOperation("Изменить рейтинг исполнителя с учетом последней оыенки и количества задач с оценкой")
    @PutMapping("/{workerId}/rating")
    fun updateWorkerRating(
            @ApiParam("ID исполнителя")
            @PathVariable workerId: UUID,
            @ApiParam("Оценка исполнителю")
            @RequestParam mark: Byte,
            @ApiParam("Количество задач исполнителя с существующей оценкой")
            @RequestParam countOfWorkerTasks:Int) = userService.updateWorkerRating(workerId, mark, countOfWorkerTasks)
}
