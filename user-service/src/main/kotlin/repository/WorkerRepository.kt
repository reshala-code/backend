package ru.reshala.userservice.repository

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import ru.reshala.userservice.model.Worker
import java.util.*

@Repository
interface WorkerRepository : PagingAndSortingRepository<Worker, UUID>{
    fun findAllByStatus(status: String?): List<Worker>
    override fun findAll(): List<Worker>
    fun findOneById(workerId:UUID): Worker?
}
