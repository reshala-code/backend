package ru.reshala.userservice.repository

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import ru.reshala.userservice.model.User
import ru.reshala.userservice.model.Worker
import java.util.*

@Repository
interface UserRepository : PagingAndSortingRepository<User,UUID>{
    fun findOneById(id: UUID): User?
    override fun findAll(): List<User>
}
