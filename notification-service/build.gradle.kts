dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-mongodb")
//    implementation("org.springframework.security:spring-security-oauth2-resource-server")
//    implementation("org.springframework.security:spring-security-oauth2-jose")
    implementation("org.springframework.cloud:spring-cloud-starter-netflix-eureka-client")
//    implementation("org.springframework.boot:spring-boot-starter-security")
//    implementation("org.keycloak:keycloak-spring-boot-starter:12.0.4")

    implementation("org.springframework.boot:spring-boot-starter-webflux")
//    implementation("org.springframework:spring-boot-starter-webflux")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")

    implementation("org.springframework.cloud:spring-cloud-starter-security")
    implementation("org.keycloak:keycloak-spring-boot-starter:12.0.4")
    implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server")
    implementation("org.springframework.boot:spring-boot-starter-oauth2-client")
    implementation("org.springframework.security:spring-security-oauth2-jose")

}
