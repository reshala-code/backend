import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import ru.reshala.notificationservice.NotificationApplication
import ru.reshala.notificationservice.controller.NotificationController
import ru.reshala.notificationservice.model.Task
import java.util.*

@SpringBootTest(classes = [NotificationApplication::class, NotificationController::class],
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ReceiveTest {

    @Autowired
    lateinit var notificationController: NotificationController

    @Test
    fun receiveTest() {
        val implementerID = UUID.fromString("f9a1f21d-f9c2-4011-9111-d726dfabec44")
        val creatorId = UUID.fromString("115ef406-b80a-4dcd-8f6d-2959d7c33d55")
        notificationController.getNotification().doOnNext{ println(it)}
        val task = Task(4,
            "dfs",
            322.32,
            emptyList(),
            null,
            creatorId)
        notificationController.receiveTask(task)
        println("kek")
    }
}
