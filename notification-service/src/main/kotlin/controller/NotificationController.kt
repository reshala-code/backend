package ru.reshala.notificationservice.controller

import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.springframework.http.MediaType
import org.springframework.http.codec.ServerSentEvent
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.DirectProcessor
import reactor.core.publisher.Flux
import reactor.core.publisher.FluxProcessor
import reactor.core.publisher.FluxSink
import ru.reshala.notificationservice.model.Notification
import ru.reshala.notificationservice.model.NotificationType
import ru.reshala.userservice.utlis.CurrentUser
import java.time.Duration
import javax.annotation.PostConstruct


//import org.springframework.messaging.SubscribableChannel


@RestController
@CrossOrigin
@RequestMapping("/notification")
class NotificationController {

    lateinit var processor: FluxProcessor<Any, Any>
    lateinit var sink: FluxSink<Any>

    lateinit var notificationProcessor: FluxProcessor<Notification, Notification>
    lateinit var notificationSink: FluxSink<Notification>

    @PostConstruct
    fun init() {
        processor = DirectProcessor.create<Any>().serialize()
        sink = processor.sink()
        notificationProcessor = DirectProcessor.create<Notification>().serialize()
        notificationSink = notificationProcessor.sink()
    }

    @ApiOperation("Отдает уведомления")
    @GetMapping(produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun getNotification(): Flux<ServerSentEvent<Notification>> {
        val userId = CurrentUser.userId
        println(userId)

        val returning = notificationProcessor.filter {
            it.belongsTo.toString() == userId
        }
            .map { e -> ServerSentEvent.builder(e).retry(Duration.ofMillis(30000)).build() }
        returning.map { print(it.data()) }
        return returning
    }

    private fun shouldNotify(customer: Boolean, worker: Boolean, notification: Notification) : Boolean {
        when (notification.type) {
            NotificationType.WORKER_FOUND -> if (customer) return true else return false
            NotificationType.TASK_FOUND -> if (worker) return true else return false
            NotificationType.TASK_CANCELLED_BY_CUSTOMER -> if (worker) return true else return false
            else -> return true
        }
    }

    @ApiOperation("Добавить событие в очередь на отправку пользователям")
    @PostMapping
    fun receiveTask(
        @ApiParam("Объект содержащий в себе информацию о событии")
        @RequestBody notification: Notification) {
        println("received:" + notification)
        notificationSink.next(notification)
    }

    fun getRandomString(length: Int): String {
        val allowedChars = ('A'..'Z') + ('a'..'z') + ('0'..'9')
        return (1..length)
            .map { allowedChars.random() }
            .joinToString("")
    }

}

data class Message(val nomer: Long, val txt: String) {


}
