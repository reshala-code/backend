package ru.reshala.notificationservice.model

import java.util.*

data class Notification(
    val type: NotificationType,
    val belongsTo: String,
    val data: Any,
)

enum class NotificationType {
    /**
     * Уведомление о том что найдена задача(по идее предназначена для Worker)
     */
    TASK_FOUND,

    /**
     * Уведомление о том, что найден исполнитель(для Customer'а)
     */
    WORKER_FOUND,

    /**
     * Уведомление о том, что статус заказа изменен (для Customer'a)
     */
    TASK_STATUS_CHANGED,

    /**
     * Уведомление о  том, что позиция Worker'a изменена (для Customer'a)
     */
    WORKER_POSITION_CHANGED,

    /**
     * Уведомление о том, что работник достиг какой-то точки
     */
    WORKER_REACHED_ADDRESS,
    TASK_CANCELLED_BY_WORKER,
    TASK_CANCELLED_BY_CUSTOMER,
    TASK_FINISHED,
    MESSAGE_RECEIVED
}
