package ru.reshala.notificationservice.model

class Device(
    val platform: String,
    val token: String
)
