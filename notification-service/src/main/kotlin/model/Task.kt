package ru.reshala.notificationservice.model

import java.util.*


class Task(
    var id: Long = 0,
    var type: String?,
    var cost: Double?,
    val addresses: List<Address>,
    var implementerId: UUID?,
    var creatorId: UUID?,
    var status: TaskStatus?,
    var comment: String?,
    var category: TaskCategory?,
)

class TaskCategory(
    var name: String = "common category",
    var basePrice: Int = 80,
    var costPerMinute: Int = 5,
    var costPer100Meter: Int = 8,
)

data class Address(
    val addressName: String?,
    var latitude: Double?,
    var longitude: Double?,
)

enum class TaskStatus {
    CREATED, IN_FINDING, CANCELLED_BY_CUSTOMER, CANCELLED_BY_WORKER, ON_THE_WAY, IN_PROGRESS, FINISHED, NOT_STARTED_INSUFFICIENT_FUNDS, IMPLEMENTER_NOT_FOUND
}

