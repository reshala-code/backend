package ru.reshala.notificationservice.model

import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "books")
class User(
    @Indexed
    val id: ObjectId?,
    val userID: Long,
    val devices: List<Device>,
    val messages: List<String>,
) {
}
