package ru.reshala.notificationservice.repository

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import ru.reshala.notificationservice.model.User

@Repository
interface UserRepository : MongoRepository<User, String>, RepositoryCustom{

}
