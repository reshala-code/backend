package ru.reshala.notificationservice.repository

interface RepositoryCustom {
    fun upsertDevice();
    fun upsertUser();
    fun saveDevice()
}
