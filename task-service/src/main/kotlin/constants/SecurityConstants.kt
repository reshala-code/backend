package ru.reshala.taskservice.constants

object SecurityConstants {
    const val ROLE_ADMIN = "ROLE_ADMIN"
    const val ROLE_CUSTOMER = "ROLE_CUSTOMER"
}
