package ru.reshala.taskservice.configuration

import org.springframework.amqp.core.*

import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer

import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import ru.reshala.taskservice.taskreceiver.TaskReceiver
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter
import org.springframework.amqp.support.converter.MessageConverter
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory

import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer
import org.springframework.http.converter.HttpMessageConverter
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule

import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.amqp.core.TopicExchange








const val topicExchangeName = "task-exchange"

const val queueName = "task-queue"
const val prioritizedQueueName = "prioritized-task-queue"


@Configuration
class TaskReceiverConfiguration {


    @Bean
    fun taskQueue(): Queue{
        return Queue(queueName, true)
    }

    @Bean
    fun prioritizedTaskQueue(): Queue {
        return Queue(prioritizedQueueName, true)
    }

    @Bean
    fun exchange(): TopicExchange? {
        return TopicExchange(topicExchangeName)
    }

    @Bean
    fun binding1(exchange: TopicExchange?): Binding? {
        return BindingBuilder.bind(taskQueue()).to(exchange).with(taskQueue().getName())
    }

    @Bean
    fun binding2(exchange: TopicExchange?): Binding? {
        return BindingBuilder.bind(prioritizedTaskQueue()).to(exchange).with(prioritizedTaskQueue().getName())
    }

    @Bean
    fun jsaFactory(
        connectionFactory: ConnectionFactory?,
        configurer: SimpleRabbitListenerContainerFactoryConfigurer,
    ): SimpleRabbitListenerContainerFactory? {
        val factory = SimpleRabbitListenerContainerFactory()
        configurer.configure(factory, connectionFactory)
        factory.setMessageConverter(converter())
        return factory
    }

    @Bean
    fun converter(): Jackson2JsonMessageConverter? {
        val objectMapper = ObjectMapper()
        objectMapper.registerModule(JavaTimeModule())
        return Jackson2JsonMessageConverter(objectMapper)
    }


}
