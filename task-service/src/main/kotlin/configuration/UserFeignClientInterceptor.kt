package ru.reshala.taskservice.configuration

import feign.RequestInterceptor
import feign.RequestTemplate
import feign.Response
import org.keycloak.KeycloakPrincipal
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken
import org.springframework.context.annotation.Bean
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient
import org.springframework.stereotype.Component
import java.lang.String
import feign.codec.ErrorDecoder
import org.springframework.http.HttpStatus
import ru.reshala.taskservice.exception.BadRequestException
import ru.reshala.taskservice.exception.ServerException


class CustomDefaultFeignConfiguration {

    @Bean
    fun getRequestInterceptor(): RequestInterceptor {
        return UserFeignClientInterceptor()
    }

    @Bean
    fun errorDecoder(): ErrorDecoder {
        return CustomErrorDecoder()
    }

}


class UserFeignClientInterceptor : RequestInterceptor {
    override fun apply(template: RequestTemplate) {
        val securityContext: SecurityContext = SecurityContextHolder.getContext()
        val authentication: Authentication? = securityContext.authentication
        if (authentication != null && authentication is KeycloakAuthenticationToken) {
            val tokenString = (authentication.principal as KeycloakPrincipal<*>).keycloakSecurityContext.tokenString
            template.header(AUTHORIZATION_HEADER, String.format("%s %s", BEARER_TOKEN_TYPE, tokenString))
        }
    }

    companion object {
        private const val AUTHORIZATION_HEADER = "Authorization"
        private const val BEARER_TOKEN_TYPE = "Bearer"
    }
}

class CustomErrorDecoder : ErrorDecoder {
    override fun decode(methodKey: kotlin.String?, response: Response): Exception {
        val requestUrl: kotlin.String = response.request().url()
        val responseBody: Response.Body = response.body()
        val responseStatus: HttpStatus = HttpStatus.valueOf(response.status())
        return if (responseStatus.is5xxServerError()) {
            ServerException(responseBody.toString())
        } else if (responseStatus.is4xxClientError()) {
            BadRequestException(responseBody.toString())
        } else {
            Exception("Generic exception")
        }
    }
}
