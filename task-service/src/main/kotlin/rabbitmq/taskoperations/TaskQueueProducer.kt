package ru.reshala.taskservice.taskoperations

import org.springframework.amqp.core.AmqpTemplate
import org.springframework.stereotype.Component
import ru.reshala.taskservice.configuration.topicExchangeName
import ru.reshala.taskservice.model.Task


@Component
class TaskQueueProducer(val amqpTemplate: AmqpTemplate) {

    private val tasksRoutingKey: String = "task-queue"
    private val refusedTasksRoutingKey: String = "prioritized-task-queue"

    fun produce(task: Task) {
        amqpTemplate.convertAndSend(topicExchangeName, tasksRoutingKey, task)
        println("Send task to queue = $task")
    }

    fun produceToPrioritizedQueue(task: Task){
        amqpTemplate.convertAndSend(topicExchangeName,refusedTasksRoutingKey,task)
    }
}
