package ru.reshala.taskservice.taskreceiver

import mu.KotlinLogging
import org.springframework.amqp.AmqpRejectAndDontRequeueException
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Component
import ru.reshala.taskservice.client.Notification
import ru.reshala.taskservice.client.NotificationClient
import ru.reshala.taskservice.client.NotificationType
import ru.reshala.taskservice.model.Task
import ru.reshala.taskservice.model.TaskStatus
import ru.reshala.taskservice.service.interfaces.TaskService
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime

private val logger = KotlinLogging.logger { }

@Component
class TaskReceiver(
    val taskService: TaskService,
    val notificationClient: NotificationClient
) {
    /**
     * Обработка задачи полученной из очереди.
     *
     */
    @RabbitListener(queues = ["task-queue","prioritized-task-queue"],)
    fun receiveTask(task: Task) {
        println("received" + task)
        // повторять в течение 5 минут // или пока 5 минут не прошло
        val workerId = taskService.findSuitableWorker(task)
        // пока newTask.implementerId не станет не налл
        val findingDuration = Duration.between(task.findingStartedDate, LocalDateTime.now())
        if (workerId == null || findingDuration> Duration.ofMinutes(5)) {
            val newTask = taskService.setStatus(task.id, TaskStatus.IMPLEMENTER_NOT_FOUND)
            //todo как вернусть в очередь
            println("Не нашли исполнителя")
            val notification = Notification(newTask.creatorId.toString(), NotificationType.WORKER_NOT_FOUND, newTask)
            notificationClient.sendNotification(notification)
            throw AmqpRejectAndDontRequeueException("Не найдено ни одного возможного исполнителя, либо прошел таймаут для поиска")
        }
        notificationClient.sendNotification(Notification(workerId.toString(), NotificationType.TASK_FOUND, task))
    }
}
