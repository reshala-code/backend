package ru.reshala.taskservice.model.converters

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.google.common.primitives.Chars.join
import org.springframework.core.io.buffer.DataBufferUtils.join
import org.springframework.util.StringUtils
import ru.reshala.taskservice.model.Address
import java.lang.String.join
import java.util.ArrayList

import javax.persistence.AttributeConverter
import javax.persistence.Converter
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import javax.annotation.PostConstruct


@Converter
class AddressListToStringConverter : AttributeConverter<MutableList<Address>, String> {
    val objectMapper = ObjectMapper();

    @PostConstruct
    fun setUp() {
        objectMapper.registerModule(JavaTimeModule())
    }

    override fun convertToDatabaseColumn(attribute: MutableList<Address>?): String {
        return objectMapper.writeValueAsString(attribute);
    }

    override fun convertToEntityAttribute(dbData: String?): MutableList<Address> {
        return objectMapper.readValue(dbData!!)
    }
}
