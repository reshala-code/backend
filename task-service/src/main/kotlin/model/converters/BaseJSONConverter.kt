package ru.reshala.taskservice.model.converters

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper


abstract class BaseJSONConverter {
    companion object {
        val mapper: ObjectMapper  = ObjectMapper()

        init {
            mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
        }
    }
}
