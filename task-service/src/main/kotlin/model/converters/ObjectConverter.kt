package ru.reshala.taskservice.model.converters

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import mu.KotlinLogging
import javax.persistence.AttributeConverter

private val logger = KotlinLogging.logger { }

class ObjectConverter : BaseJSONConverter(), AttributeConverter<Any?, String?> {
    override fun convertToDatabaseColumn(attribute: Any?): String? {
        val mapper: ObjectMapper = mapper
        return if (attribute == null) {
            return ""
        } else try {
            return mapper.writeValueAsString(attribute)
        } catch (e: JsonProcessingException) {
            logger.error("Exception while converting to database column", e)
            null
        }
    }

    override fun convertToEntityAttribute(dbData: String?): Any? {
        val mapper: ObjectMapper = mapper
        return try {
            if (dbData.isNullOrBlank()) {
                return null
            }
            mapper.readValue(dbData, Any::class.java)
        } catch (e: Exception) {
            logger.error("Exception while converting to entity attribute", e)
            null
        }
    }

}
