package ru.reshala.taskservice.model.converters

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import ru.reshala.taskservice.model.Address
import java.util.*
import javax.persistence.AttributeConverter

class UUIDListToStringConverter : AttributeConverter<MutableList<UUID>, String> {
    val objectMapper = ObjectMapper();

    override fun convertToDatabaseColumn(attribute: MutableList<UUID>?): String {
        return objectMapper.writeValueAsString(attribute);
    }

    override fun convertToEntityAttribute(dbData: String?): MutableList<UUID> {
        return objectMapper.readValue(dbData!!)
    }
}
