package ru.reshala.taskservice.model

import org.springframework.data.annotation.CreatedDate
import ru.reshala.taskservice.model.TaskStatus.*
import ru.reshala.taskservice.model.converters.AddressListToStringConverter
import ru.reshala.taskservice.model.converters.ObjectConverter
import ru.reshala.taskservice.model.converters.UUIDListToStringConverter
import ru.reshala.taskservice.utils.CurrentUser
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*
import javax.validation.constraints.Max
import javax.validation.constraints.Min

@Entity
class Task(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0,
    var cost: Int?,
    var finalCost: Int?,
    @Convert(converter = AddressListToStringConverter::class)
    @Column(columnDefinition = "text")
    var addresses: List<Address>,
    var implementerId: UUID?,
    var creatorId: UUID = UUID.fromString(CurrentUser.userId),
    @Enumerated(EnumType.STRING)
    var status: TaskStatus = CREATED,
    var comment: String?,
    @Enumerated(EnumType.STRING)
    var type: TaskCategoryName?,
    @Convert(converter = ObjectConverter::class)
    @Column(columnDefinition = "text")
    var categoryInfo: Any?,
    @Convert(converter = UUIDListToStringConverter::class)
    @Column(columnDefinition = "text")
    var refusedWorkers: MutableList<UUID> = mutableListOf(),
    var promocode: String? = null,
    @CreatedDate
    val createdDate: LocalDateTime = LocalDateTime.now(),
    var findingStartedDate: LocalDateTime? = null,

    @Min(0)
    @Max(5)
    var rating: Byte? = null
) {
}

enum class TaskCategoryName {
    COURIER, CLEANER, LOADER, GARBAGE
}


@Embeddable
data class Address(
    val addressName: String?,
    var latitude: Double?,
    var longitude: Double?,
    var isChecked: Boolean = false,
    @Embedded
    var checkedInfo: CheckInInfo? = CheckInInfo(),
)

data class CleanerCategoryInfo(
    val squareMeters: Double,
    val isCleaningWindowOn: Boolean = false,
) : CategoryInfo()

data class LoaderCategoryInfo(
    val minutes: Double,
) : CategoryInfo()

data class GarbageCategoryInfo(
    val bagCount: Double,
) : CategoryInfo()


open class CategoryInfo {

}

@Embeddable
class CheckInInfo(
    var checkTime: LocalDateTime? = null,
    var realLatitude: Double? = null,
    var realLongitude: Double? = null,
)

enum class TaskStatus {
    CREATED, IN_FINDING, CANCELLED_BY_CUSTOMER, CANCELLED_BY_WORKER, ON_THE_WAY, IN_PROGRESS, FINISHED, NOT_STARTED_INSUFFICIENT_FUNDS, IMPLEMENTER_NOT_FOUND
}
