package ru.reshala.taskservice.model

import java.util.*


data class User(
    val id: UUID?=null,
    val name: String?=null,
)
