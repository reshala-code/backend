package ru.reshala.taskservice.model

import java.util.*

data class Worker(
    val id: UUID,
    val name: String?,
    val photo: String?,
    var status: String?,
    var latitude: Double?,
    var longitude: Double?
)
