package ru.reshala.taskservice.model

class TaskForUser(
    var id: Long?,
    var cost: Int?,
    var finalCost: Int?,
    val addresses: List<Address>,
    var status: TaskStatus,
    var comment: String?,
    var type: TaskCategoryName?,
) {
    var customer: TaskCustomer? = null
    var worker: TaskWorker? = null

    constructor(task: Task) : this(task.id,
        task.cost,
        task.finalCost,
        task.addresses,
        task.status,
        task.comment,
        task.type) {

    }
}

class TaskWorker {
    var id: String? = null
    var firstName: String? = null
    var lastName: String? = null
    var patronymic: String? = null
    var phoneNumber: String? = null
    var photo: String? = null
    var rating: Double? = null

    constructor(
        id: String?,
        firstName: String?,
        lastName: String?,
        patronymic: String?,
        phoneNumber: String?,
        photo: String?,
        rating: Double?,
    ) {
        this.id = id
        this.firstName = firstName
        this.lastName = lastName
        this.patronymic = patronymic
        this.phoneNumber = phoneNumber
        this.photo = photo
        this.rating = rating
    }

    constructor()


}

class TaskCustomer {
    var id: String? = null
    var firstName: String? = null
    var lastName: String? = null
    var phoneNumber: String? = null

    constructor(id: String?, firstName: String?, lastName: String?, phoneNumber: String?) {
        this.id = id;
        this.firstName = firstName
        this.lastName = lastName
        this.phoneNumber = phoneNumber
    }
}
