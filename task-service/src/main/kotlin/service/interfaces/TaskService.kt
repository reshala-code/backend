package ru.reshala.taskservice.service.interfaces

import ru.reshala.taskservice.model.Task
import ru.reshala.taskservice.model.TaskStatus
import java.util.*

interface TaskService {
    fun getAllByUserId(userId: UUID): List<Task>
    fun getByWorkerId(workerId: UUID): Task?
    fun getById(taskId: Long): Task?
    fun save(task: Task): Task
    fun setStatus(taskId: Long, status: TaskStatus): Task
    fun findSuitableWorker(task: Task): UUID?
//    fun addTaskToQueue(taskId: Long)
    fun startFindImplementer(taskId: Long)
    fun getTaskByCreatorId(creatorId: UUID): Task?
    fun addRefusedWorker(taskId: Long, workerId: UUID)
    fun prepareTask(task: Task, userId: UUID) : Task
    fun checkIn(taskId: Long, addressIndex: Int, latitude: Double, longitude: Double)
    fun finishTask(taskId: Long,latitude: Double, longitude: Double)
    fun rateWorker(taskId: Long, mark: Byte)
}
