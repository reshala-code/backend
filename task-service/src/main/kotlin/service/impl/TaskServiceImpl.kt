package ru.reshala.taskservice.service.impl

import feign.FeignException
import mu.KotlinLogging
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Isolation
import org.springframework.transaction.annotation.Transactional
import ru.reshala.taskservice.client.*
import ru.reshala.taskservice.client.HereMapsTransportMode.PEDESTRIAN_WITHOUT_TRAFFIC
import ru.reshala.taskservice.client.RouteInfoResponse.Route
import ru.reshala.taskservice.exception.BadRequestException
import ru.reshala.taskservice.exception.NotFoundException
import ru.reshala.taskservice.model.Task
import ru.reshala.taskservice.model.TaskForUser
import ru.reshala.taskservice.model.TaskStatus
import ru.reshala.taskservice.model.TaskStatus.*
import ru.reshala.taskservice.repository.TaskRepository
import ru.reshala.taskservice.service.interfaces.TaskService
import ru.reshala.taskservice.taskoperations.TaskQueueProducer
import ru.reshala.taskservice.utils.CurrentUser
import java.time.LocalDateTime
import java.util.*
import kotlin.Comparator

private val logger = KotlinLogging.logger { }

@Service
class TaskServiceImpl(
    val taskRepository: TaskRepository,
    val userClient: UserClient,
    val hereMapsClient: HereMapsClient,
    val geoClient: GeoClient,
    val queueProducer: TaskQueueProducer,
    val transactionClient: TransactionClient,
    val notificationClient: NotificationClient,
    val chatClient: ChatClient,
) : TaskService {

    override fun getAllByUserId(userId: UUID): List<Task> {
        lateinit var userServiceResponse: UserServiceResponse
        try {
            userServiceResponse = userClient.findByUserId(userId)
        } catch (e: FeignException.BadRequest) {
            //log
            return emptyList()
        }

        return when (userServiceResponse.type) {
            null -> taskRepository.findAll()
            "Worker" -> return taskRepository.findAllByImplementerId(userServiceResponse.user.id!!)
            else -> taskRepository.findAllByCreatorId(userServiceResponse.user.id!!)
        }

    }

    override fun getById(taskId: Long) = taskRepository.findOneById(taskId)

    override fun save(task: Task): Task {
        return taskRepository.save(task)
    }

    override fun setStatus(taskId: Long, status: TaskStatus): Task {
        val task = taskRepository.findByIdOrNull(taskId) ?: throw BadRequestException("Такая задача не найдена")
        return when (status) {
            FINISHED -> finish(task)
            CANCELLED_BY_CUSTOMER, CANCELLED_BY_WORKER -> cancel(task, status)
            IN_FINDING -> return task// todo
            IN_PROGRESS -> changeTaskStatus(taskId, status) // воркер дошел до точки и ждет чего-то
            NOT_STARTED_INSUFFICIENT_FUNDS -> changeTaskStatus(taskId, NOT_STARTED_INSUFFICIENT_FUNDS)
            IMPLEMENTER_NOT_FOUND -> {
                cancel(task, status)
            }
            ON_THE_WAY -> start(task)
            else -> throw BadRequestException("Недопустимый статус")
        }
    }

    private fun finish(task: Task): Task {
        val changingTask = task.takeIf { it.status == IN_PROGRESS }
            ?: throw BadRequestException("Недопустимое значение для перевода задачи в FINISHED")
        changingTask.status = FINISHED
        val returningTask = taskRepository.save(changingTask)

        userClient.changeWorkerStatus(changingTask.implementerId, "Free")
        transactionClient.inProgress(entityId = task.id.toString(), type = TransactionType.TASK_PAY)

        return returningTask
    }

    private fun cancel(task: Task, status: TaskStatus): Task {
        val changingTask = task.takeIf { it.status == ON_THE_WAY || it.status == IN_FINDING }
            ?: throw BadRequestException("Недопустимое значение для перевода задачи в CANCELLED")

        changingTask.status = status
        val savedTask = taskRepository.save(changingTask)
        transactionClient.cancel(entityId = task.id.toString(), type = TransactionType.TASK_PAY)
        var notification: Notification
        if (status == CANCELLED_BY_WORKER) {
            notification = Notification(task.creatorId.toString(), NotificationType.TASK_CANCELLED_BY_WORKER, savedTask)
            userClient.changeWorkerStatus(task.implementerId, "Free")
            notificationClient.sendNotification(notification)
        }
        if (status == CANCELLED_BY_CUSTOMER && task.implementerId != null) {
            notification = Notification(task.implementerId.toString(), NotificationType.TASK_CANCELLED_BY_CUSTOMER, savedTask)
            userClient.changeWorkerStatus(task.implementerId, "Free")
            notificationClient.sendNotification(notification)
        }
        if (status == IMPLEMENTER_NOT_FOUND) {
            notification = Notification(task.creatorId.toString(), NotificationType.WORKER_NOT_FOUND, savedTask)
            notificationClient.sendNotification(notification)
        }

        return savedTask
    }

    override fun checkIn(taskId: Long, addressIndex: Int, latitude: Double, longitude: Double) {
        val task = taskRepository.findOneById(taskId) ?: throw NotFoundException("unable to find task with id: $taskId")
            .also { logger.debug { "unable to find task with id: $taskId" } }
        if (task.status !in listOf(ON_THE_WAY, IN_PROGRESS)) {
            throw BadRequestException("Недопустимое действие для задачи со статусом ${task.status} ")
        }

        task.addresses[addressIndex].isChecked = true
        task.addresses[addressIndex].checkedInfo?.checkTime = LocalDateTime.now()
        task.addresses[addressIndex].checkedInfo?.realLatitude = latitude
        task.addresses[addressIndex].checkedInfo?.realLongitude = longitude
        taskRepository.save(task)

        if (addressIndex == 0) {
            setStatus(taskId, IN_PROGRESS)
        }
        val notification = Notification(task.creatorId.toString(), NotificationType.WORKER_REACHED_ADDRESS, addressIndex)
        notificationClient.sendNotification(notification)
    }

    override fun finishTask(taskId: Long, latitude: Double, longitude: Double) {
        val task = taskRepository.findOneById(taskId) ?: throw NotFoundException("unable to find task with id: $taskId")
            .also { logger.debug { "unable to find task with id: $taskId" } }
        setStatus(taskId, FINISHED)
        val notification = Notification(task.creatorId.toString(), NotificationType.TASK_FINISHED, task)
        notificationClient.sendNotification(notification)
        return
    }

    override fun rateWorker(taskId: Long, mark: Byte) {
        val task = taskRepository.findOneById(taskId) ?: throw NotFoundException("unable to find task with id: $taskId")
                .also { logger.debug { "unable to find task with id: $taskId" } }

        if (UUID.fromString(CurrentUser.userId) != task.creatorId){
            throw BadRequestException("Вы не являетесь создателем данной задачи")
        }

        if (task.status != FINISHED || task.rating != null){
            throw BadRequestException("Данная задача уже оценена или еще не завершена")
        }

        if (mark<0 || mark>5){
            throw BadRequestException("Недопустимое значение для оценки")
        }

        task.rating = mark

        taskRepository.save(task)

        val countOfWorkerTasksWithExistingRating = taskRepository.countByImplementerIdAndRatingNotNull(task.implementerId!!)

        this.userClient.updateWorkerRating(task.implementerId!!, mark, countOfWorkerTasksWithExistingRating)

    }

    private fun start(task: Task): Task {
        val changingTask = task.takeIf { it.status == IN_FINDING }
            ?: throw BadRequestException("Недопустимое значение для перевода задачи в ON THE WAY")
        val futureWorkerId = UUID.fromString(CurrentUser.userId)
        userClient.changeWorkerStatus(futureWorkerId, "Working")
        changingTask.implementerId = futureWorkerId
        changingTask.status = ON_THE_WAY
        taskRepository.save(changingTask)
        val taskForWorker = TaskForUser(task)
        val notification = Notification(task.creatorId.toString(), NotificationType.WORKER_FOUND, taskForWorker)
        notificationClient.sendNotification(notification)
        chatClient.createRoom(changingTask.implementerId.toString(), changingTask.creatorId.toString())
        return task
    }

    /**
     * Находит соответсвующего исполнителя и возвращает его ID
     */
    override fun findSuitableWorker(task: Task): UUID? {
        val allFreeWorkers = userClient.getWorkers("Free").filter {
            !task.refusedWorkers.contains(it.id)
        }
        val freeWorkersIds = allFreeWorkers.map { it.id }.toSet()
        val closeWorkers =
            geoClient.findSuitableWorker(task.addresses[0].latitude!!, task.addresses[0].longitude!!)
                .filter {
                    freeWorkersIds.contains(it.userId)
                }
        val destination = getInHereMapsCoordinatesFormat(task.addresses[0].latitude!!, task.addresses[0].longitude!!)

        val suitableWorker = getWorkerWithExistingMinRoute(closeWorkers, destination)

        return suitableWorker?.userId
    }

    override fun startFindImplementer(taskId: Long) {
        val task = taskRepository.findOneById(taskId)
            ?: throw NotFoundException("unable to find task with id: $taskId")
                .also { logger.debug { "unable to find task with id: $taskId" } }
        try {
            transactionClient.openTransaction(
                task.creatorId.toString(),
                null,
                taskId.toString(),
                task.finalCost!!.toDouble(),
                TransactionType.TASK_PAY)
        } catch (ex: Exception) {
            task.status = NOT_STARTED_INSUFFICIENT_FUNDS
            taskRepository.save(task)
            throw BadRequestException("Недостаточно средств на счете")
        }
        addTaskToQueue(taskId)
    }

    private fun addTaskToQueue(taskId: Long) {
        val task = taskRepository.findOneById(taskId)
            ?: throw NotFoundException("unable to find task with id: $taskId")
                .also { logger.debug { "unable to find task with id: $taskId" } }


        task.status = IN_FINDING
        task.findingStartedDate = LocalDateTime.now()
        taskRepository.save(task)
        // денег достаточно кидаем в очередь, там консюмер очереди найдет нам исполнителя по кайфу
        queueProducer.produce(task)
    }

    private fun getWorkerWithExistingMinRoute(closeWorkers: List<GeoUser>, destination: String): GeoUser? {
        val customComparator = Comparator<GeoUser> { a, b ->
            compareValues(getMinRoute(a, destination).getDuration(), getMinRoute(b, destination).getDuration())
        }

        return closeWorkers.minWithOrNull(customComparator)
    }


    private fun getMinRoute(geoServiceResponse: GeoUser, destination: String): Route? {
        val busRoutes = hereMapsClient.findRoute(
            wayPoints = convertToWayPointsMap(
                listOf(
                    getInHereMapsCoordinatesFormat(geoServiceResponse.latitude!!, geoServiceResponse.longitude!!),
                    destination
                )
            )
        ).routes

        val pedestrianRoutes = hereMapsClient.findRoute(
            wayPoints = convertToWayPointsMap(
                listOf(
                    getInHereMapsCoordinatesFormat(geoServiceResponse.latitude, geoServiceResponse.longitude),
                    destination
                )
            ),
            mode = PEDESTRIAN_WITHOUT_TRAFFIC.value
        ).routes

        return if (busRoutes.isEmpty() && pedestrianRoutes.isEmpty())
            null
        else
            if (busRoutes[0].summary.travelTime.compareTo(
                    pedestrianRoutes[0].summary.travelTime
                ) == -1
            ) busRoutes[0]
            else pedestrianRoutes[0]
    }

    override fun getByWorkerId(workerId: UUID): Task? {
        val tasks = taskRepository.findByImplementerIdAndStatusInOrderById(workerId, listOf(ON_THE_WAY, IN_PROGRESS))
        if (tasks.isEmpty()) {
            logger.error { "Not found any running task for worker: $workerId" }
            return null
        }
        if (tasks.size > 1) {
            logger.error { "Found more then one running task for worker: $workerId . Returning last" }
            return tasks.last()
        }
        return tasks.first()
    }

    override fun getTaskByCreatorId(creatorId: UUID): Task? {
        val tasks = taskRepository.findByCreatorIdAndStatusInOrderById(creatorId, listOf(IN_PROGRESS, ON_THE_WAY))
        if (tasks.isEmpty()) {
            logger.error { "Not found any running task for creator: $creatorId" }
            return null
        }
        if (tasks.size > 1) {
            logger.error { "Found more then one running task for creator: $creatorId . Returning last" }
            return tasks.last()
        }
        return tasks.first()
    }

    private fun changeTaskStatus(taskId: Long, taskStatus: TaskStatus): Task {
        val task = taskRepository.findOneById(taskId) ?: throw NotFoundException("unable to find task with id: $taskId")
            .also { logger.debug { "unable to change task with id: $taskId" } }
        task.status = taskStatus
        return taskRepository.save(task)
    }

    override fun addRefusedWorker(taskId: Long, workerId: UUID) {
        val task = taskRepository.findOneById(taskId) ?: throw NotFoundException("Unable to find task with id: $taskId")
        task.refusedWorkers.add(workerId)
        taskRepository.save(task)
        queueProducer.produceToPrioritizedQueue(task)
    }


    @Transactional(isolation = Isolation.SERIALIZABLE)
    override fun prepareTask(task: Task, userId: UUID): Task {
        val countTaskCost = transactionClient.countTaskCost(task)
        val existingTasks = taskRepository.findByCreatorIdAndStatusOrderById(userId, CREATED)
            .ifEmpty {
                task.cost = countTaskCost.cost
                task.finalCost = countTaskCost.finalCost
                return taskRepository.save(task)
            }

        if (existingTasks.size > 1) {
            throw BadRequestException("У вас уже есть активная задача id").also {
                logger.warn("У данного пользоватля(${CurrentUser.userId}) найдено более 2 задач со статусом ${CREATED}")
            }
        }

        val currentTask = existingTasks[0]
        currentTask.addresses = task.addresses
        currentTask.cost = countTaskCost.cost
        currentTask.finalCost = countTaskCost.finalCost
        currentTask.type = task.type
        currentTask.categoryInfo = task.categoryInfo
        currentTask.promocode = task.promocode

        return taskRepository.save(currentTask)
    }

}
