package ru.reshala.taskservice.client

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import ru.reshala.taskservice.configuration.CustomDefaultFeignConfiguration
import ru.reshala.taskservice.model.Task
import java.util.*

@Component
@FeignClient(name = "transaction-service",configuration = [CustomDefaultFeignConfiguration::class])
interface TransactionClient {
    @PostMapping("/transaction")
    fun isEnoughMoneyForTask(
        @RequestBody task: Task,
    ): Boolean

    @PostMapping("/task/cost")
    fun countTaskCost(
        @RequestBody task: Task,
    ): TaskCost

    @PostMapping("/transaction/open")
    fun openTransaction(
        @RequestParam from: String? = null,
        @RequestParam to: String? = null,
        @RequestParam entityId: String? = null,
        @RequestParam value: Double,
        @RequestParam type: TransactionType,
    )

    @PostMapping("/transaction/inProgress")
    fun inProgress(
        @RequestParam transactionId: UUID? = null,
        @RequestParam entityId: String? = null,
        @RequestParam type: TransactionType? = null,
    )

    @PostMapping("/transaction/cancel")
    fun cancel(
        @RequestParam transactionId: UUID? = null,
        @RequestParam entityId: String? = null,
        @RequestParam type: TransactionType? = null,
    )
}

data class TaskCost(
    val cost: Int,
    val finalCost: Int,
)

enum class TransactionType(val hasCommission: Boolean = false) {
    TASK_PAY(true), SERVICE_PAY, DEPOSIT, WITHDRAW(true), COMMISSION, CORRECTION(true), WORKER_PAY
}
