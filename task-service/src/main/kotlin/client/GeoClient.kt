package ru.reshala.taskservice.client

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import ru.reshala.taskservice.model.Address
import java.util.*

@Component
@FeignClient(name = "geo-service",url = "localhost:8084")
interface GeoClient {
    @GetMapping("/geo")
    fun findSuitableWorker(
        @RequestParam latitude: Double,
        @RequestParam longitude: Double,
        @RequestParam distance: Double = 5.0
    ): MutableList<GeoUser>

    @GetMapping("distanceAndDuration")
    fun getDistanceAndDuration(addresses: List<Address>) : DistanceAndDuration
}

class DistanceAndDuration {
    var distance: Long? = null
    var duration: Long? = null
}

data class GeoUser(
    val userId: UUID?,
    val latitude: Double?,
    val longitude: Double?
)
