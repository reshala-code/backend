package ru.reshala.taskservice.client

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.*
import ru.reshala.taskservice.configuration.CustomDefaultFeignConfiguration
import java.util.*

@Component
@FeignClient(name = "user-service",configuration = [CustomDefaultFeignConfiguration::class])
interface UserClient {

    @GetMapping("/user/{userID}")
    fun findByUserId(@PathVariable("userID") userId: UUID): UserServiceResponse

    @GetMapping("/user/customer/{userID}")
    fun findByCustomerId(@PathVariable("userID") userId: UUID): Customer

    @GetMapping("/user/worker/{userID}")
    fun findByWorkerId(@PathVariable("userID") userId: UUID): Worker

    @GetMapping("/user/worker")
    fun getWorkers(@RequestParam status: String? = null): List<Worker>

    @PutMapping("/user/{workerId}/status")
    fun changeWorkerStatus(@PathVariable workerId: UUID?, @RequestParam status: String)

    @PutMapping("/user/{workerId}/rating")
    fun updateWorkerRating(@PathVariable workerId: UUID, @RequestParam mark: Byte, @RequestParam countOfWorkerTasks:Int)
}

data class UserServiceResponse(val user: User, val type: String?)

open class User(
    open var id: UUID?,
    open var firstName: String?,
    open var lastName: String?,
    open var phoneNumber: String?,
)

class Worker(
    override var id: UUID? = null,
    var name: String? = null,
    var photo: String?,
    var patronymic: String?,
    var rating: Double?,
    override var firstName: String?,
    override var lastName: String?,
    override var phoneNumber: String?,
) : User(id,firstName, lastName, phoneNumber)

class Customer(
    override var id: UUID? = null,
    override var firstName: String?,
    override var lastName: String?,
    override var phoneNumber: String?,
) : User(id,firstName, lastName, phoneNumber)
