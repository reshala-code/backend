package ru.reshala.taskservice.client

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import ru.reshala.taskservice.model.Task
import java.util.*

@Component
@FeignClient(name = "notification-service")
interface NotificationClient {
    @PostMapping("/notification")
    fun sendNotification(
       @RequestBody notification: Notification
    )

}

data class Notification(
    val belongsTo: String,
    val type: NotificationType,
    val data: Any,
)

enum class NotificationType {
    TASK_FOUND, WORKER_FOUND, TASK_STATUS_CHANGED, WORKER_POSITION_CHANGED, WORKER_REACHED_ADDRESS, TASK_CANCELLED_BY_WORKER, TASK_CANCELLED_BY_CUSTOMER, TASK_FINISHED, WORKER_NOT_FOUND
}
