package ru.reshala.taskservice.client

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import ru.reshala.taskservice.model.Address
import java.util.*

@Component
@FeignClient(name = "chat-service")
interface ChatClient {
    @PostMapping("/chat/create")
    fun createRoom(
        @RequestParam user1: String,
        @RequestParam user2: String,
    ): String?

}
