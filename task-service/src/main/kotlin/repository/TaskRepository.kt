package ru.reshala.taskservice.repository

import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import ru.reshala.taskservice.model.Task
import ru.reshala.taskservice.model.TaskCategoryName
import ru.reshala.taskservice.model.TaskStatus
import java.util.*

@Repository
interface TaskRepository : PagingAndSortingRepository<Task, Long> {
    fun findAllByCreatorId(creator: UUID): List<Task>
    fun findAllByImplementerId(implementerId: UUID): List<Task>
    fun findByImplementerIdAndStatusInOrderById(implementerId: UUID, status: List<TaskStatus>): List<Task>
    fun findByCreatorIdAndStatusOrderById(creatorId: UUID, status: TaskStatus): List<Task>
    fun findByCreatorIdAndStatusInOrderById(implementerId: UUID, status: List<TaskStatus>): List<Task>
    fun findByType(type: TaskCategoryName): List<Task>
    override fun findAll(): List<Task>
    fun findOneById(taskId: Long): Task?
    fun countByImplementerIdAndRatingNotNull(implementerId: UUID): Int
    fun countTasksByImplementerIdAndRatingIsNotNull(implementerId: UUID): Int
}
