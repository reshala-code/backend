package ru.reshala.taskservice

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.cloud.openfeign.EnableFeignClients

@SpringBootApplication()
@EnableFeignClients
@EnableEurekaClient
class TaskApplication

fun main(args: Array<String>) {
    SpringApplication.run(TaskApplication::class.java, *args)
}
