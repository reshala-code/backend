package ru.reshala.taskservice.exception

class BadRequestException : TaskServiceException {
    constructor(message: String) : super(message)
    constructor(message: String, exception: Exception) : super(message, exception)
}
