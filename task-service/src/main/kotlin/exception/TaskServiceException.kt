package ru.reshala.taskservice.exception

abstract class TaskServiceException: RuntimeException {
    constructor(message: String): super(message)
    constructor(message: String, exception: Exception): super(message, exception)
}
