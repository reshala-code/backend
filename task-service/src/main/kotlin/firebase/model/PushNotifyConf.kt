package ru.reshala.taskservice.firebase.model

data class PushNotifyConf (
        var title: String? = null,
        var body: String? = null,
        var icon: String? = null,
        var click_action: String? = null,
        var ttlInSeconds: String? = "100"
)