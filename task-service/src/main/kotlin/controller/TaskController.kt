package ru.reshala.taskservice.controller

import com.fasterxml.jackson.databind.ObjectMapper
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.*
import ru.reshala.taskservice.client.*
import ru.reshala.taskservice.constants.SecurityConstants.ROLE_ADMIN
import ru.reshala.taskservice.constants.SecurityConstants.ROLE_CUSTOMER
import ru.reshala.taskservice.exception.BadRequestException
import ru.reshala.taskservice.model.*
import ru.reshala.taskservice.service.interfaces.TaskService
import ru.reshala.taskservice.utils.CurrentUser
import java.lang.IllegalArgumentException
import java.sql.SQLException
import java.util.*

@RestController
@RequestMapping("/task")
@CrossOrigin
class TaskController(
    val taskService: TaskService,
    val userClient: UserClient,
    val objectMapper: ObjectMapper,
) {

    @Secured(ROLE_ADMIN)
    @ApiOperation("Получить список задач по ID пользователя")
    @GetMapping("/admin/user/{userId}")
    fun getTasksByUserId(
        @ApiParam("ID пользователя")
        @PathVariable userId: UUID,
    ) = taskService.getAllByUserId(userId)

    @ApiOperation("Получить список задач по ID пользователя")
    @GetMapping("/user/{userId}")
    fun getTasks(
        @ApiParam("ID пользователя")
        @PathVariable userId: UUID,
    ) = taskService.getAllByUserId(userId)

    @ApiOperation("Получить информацию о задаче")
    @GetMapping("/{taskId}")
    fun getTask(
        @ApiParam("ID задачи")
        @PathVariable taskId: Long,
    ) = taskService.getById(taskId)


    @ApiOperation("Получить текущую задачу пользователя")
    @GetMapping
    @Secured("ROLE_CUSTOMER", "ROLE_WORKER")
    fun getCurrentTask(): TaskForUser? {
        val task = (if (CurrentUser.isWorker) taskService.getByWorkerId(UUID.fromString(CurrentUser.userId))
        else
            taskService.getTaskByCreatorId(UUID.fromString(CurrentUser.userId)))
            ?: return null

        val taskForUser = TaskForUser(task)
        val customer = userClient.findByCustomerId(task.creatorId)
        taskForUser.customer =
            TaskCustomer(customer.id.toString(), customer.firstName, customer.lastName, customer.phoneNumber)
        val worker = userClient.findByWorkerId(task.implementerId!!)
        taskForUser.worker = TaskWorker(
            worker.id.toString(),
            worker.firstName,
            worker.lastName,
            worker.patronymic,
            worker.phoneNumber,
            worker.photo,
            worker.rating,
        )
        return taskForUser
    }

    @ApiOperation("Получить текущую задачу пользователя")
    @GetMapping("/admin/active-task/{userId}")
    @Secured(ROLE_ADMIN)
    fun getCurrentTaskById(@PathVariable("userId") userId: UUID): TaskForUser? {
        val task = taskService.getByWorkerId(userId) ?: taskService.getTaskByCreatorId(userId) ?: return null

        val taskForUser = TaskForUser(task)
        val customer = userClient.findByCustomerId(task.creatorId)
        taskForUser.customer =
            TaskCustomer(customer.id.toString(), customer.firstName, customer.lastName, customer.phoneNumber)
        val worker = userClient.findByWorkerId(task.implementerId!!)
        taskForUser.worker = TaskWorker(
            worker.id.toString(),
            worker.firstName,
            worker.lastName,
            worker.patronymic,
            worker.phoneNumber,
            worker.photo,
            worker.rating,
        )
        return taskForUser
    }

    @ApiOperation("Получить текущую задачу пользователя")
    @GetMapping("/active-task/{userId}")
    fun getCurrentTaskByUserId(@PathVariable("userId") userId: UUID): Task? {
        val task = taskService.getByWorkerId(userId) ?: taskService.getTaskByCreatorId(userId) ?: return null

        return task
    }

    @ApiOperation("Обновить данные о задаче")
    @PutMapping
    fun updateTask(
        @ApiParam("Обновленный объект задачи")
        @RequestBody task: Task,
    ) = taskService.save(task)

    @ApiOperation("Выполнить предварительное создание задачи",
        notes = "При этом не выполняется поиск исполнителя и публикация задачи. Будет возвращен объект задачи с информацией о стоимости")
    @PostMapping
    fun prepareTask(
        @ApiParam("Объект задачи")
        @RequestBody task: Task,
    ): Task {
        return try {
            if (!isTaskValid(task)) {
                throw BadRequestException("Некорректный тип задачи")
            }
            taskService.prepareTask(task, UUID.fromString(CurrentUser.userId))
        } catch (ex: SQLException) {
            println("Ошибка какая-то.")
            throw BadRequestException("какая-то SQL ошибка")
        }
    }

    private fun isTaskValid(task: Task): Boolean {
        task.categoryInfo
        try {
            when (task.type) {
                TaskCategoryName.CLEANER -> objectMapper.convertValue(task.categoryInfo,
                    CleanerCategoryInfo::class.java)
                TaskCategoryName.COURIER -> task.categoryInfo == null
                TaskCategoryName.GARBAGE -> objectMapper.convertValue(task.categoryInfo,
                    GarbageCategoryInfo::class.java)
                TaskCategoryName.LOADER -> objectMapper.convertValue(task.categoryInfo,
                    LoaderCategoryInfo::class.java)
                else -> return false
            }
            return true
        } catch (ex: IllegalArgumentException) {
            return false
        }

    }

    @ApiOperation("Принять задачу")
    @PatchMapping("/{taskId}/accept")
    fun acceptTask(
        @ApiParam("ID задачи")
        @PathVariable taskId: Long,
    ) {
        taskService.setStatus(taskId, TaskStatus.ON_THE_WAY)
    }

    @ApiOperation("Отказатья от задачи")
    @PatchMapping("/{taskId}/decline")
    fun declineTask(
        @ApiParam("ID задачи")
        @PathVariable taskId: Long,
    ) {
        val workerId = UUID.fromString(CurrentUser.userId)
        taskService.addRefusedWorker(taskId, workerId)
    }

    @ApiOperation("Отменить задачу")
    @PatchMapping("/{taskId}/cancel")
    fun cancelTask(
        @ApiParam("ID задачи")
        @PathVariable taskId: Long,
    ) {
        val status = if (CurrentUser.isCustomer) TaskStatus.CANCELLED_BY_CUSTOMER else TaskStatus.CANCELLED_BY_WORKER
        taskService.setStatus(taskId, status)
    }

    @ApiOperation("Начать поиск исполнителя по заданной задаче")
    @PostMapping("/{taskId}/start-finding-implementer")
    fun startFindImplementer(
        @ApiParam("ID задачи")
        @PathVariable taskId: Long,
    ) = taskService.startFindImplementer(taskId)

    @ApiOperation("Отметить, что исполнитель прибыл на указанный адрес")
    @PatchMapping("/{taskId}/check-in/{addressIndex}")
    fun checkIn(
        @ApiParam("ID задачи")
        @PathVariable taskId: Long,
        @ApiParam("Порядковый номер адреса, к которому прибыл исполнитель. Отсчет от 0")
        @PathVariable addressIndex: Int,
        @ApiParam("Широта")
        @RequestParam longitude: Double,
        @ApiParam("Долгота")
        @RequestParam latitude: Double,
    ) {
        taskService.checkIn(taskId, addressIndex, latitude, longitude)
    }

    @ApiOperation("Завершить задачу со стороны исполнителя")
    @PatchMapping("/{taskId}/finish")
    fun finishTask(
        @ApiParam("ID задачи")
        @PathVariable taskId: Long,
        @ApiParam("Широта")
        @RequestParam longitude: Double,
        @ApiParam("Долгота")
        @RequestParam latitude: Double,
    ) {
        taskService.finishTask(taskId, latitude, longitude)
    }

    @ApiOperation("Оценить исполнителя задачи")
    @PutMapping("/{taskId}/rating")
    @Secured(ROLE_CUSTOMER)
    fun rateWorker(
            @ApiParam("ID задачи")
            @PathVariable taskId: Long,
            @ApiParam("Оценка")
            @RequestParam mark: Byte) =  taskService.rateWorker(taskId,mark)

//
//    @PatchMapping("/{taskId}/status")
//    fun changeStatus(@PathVariable taskId: Long, @RequestParam status: TaskStatus) {
//        taskService.setStatus(taskId, status)
//    }
}




