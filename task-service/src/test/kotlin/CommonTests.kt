import org.junit.jupiter.api.Test

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import ru.reshala.taskservice.TaskApplication
import ru.reshala.taskservice.client.HereMapsClient
import ru.reshala.taskservice.client.HereMapsTransportMode
import ru.reshala.taskservice.client.convertToWayPointsMap
import ru.reshala.taskservice.client.getInHereMapsCoordinatesFormat

@SpringBootTest(classes = [TaskApplication::class],
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class KotlinDemoApplicationTests {

    @Autowired
    lateinit var hereClient: HereMapsClient

    @Test
    fun testHereCall() {
        print("kek")
        val findRoute = hereClient.findRoute(
            wayPoints = convertToWayPointsMap(listOf(getInHereMapsCoordinatesFormat(55.760201, 37.629807),
            getInHereMapsCoordinatesFormat(55.784617,37.850299),
                    getInHereMapsCoordinatesFormat(55.760201, 37.633807))),
                mode = HereMapsTransportMode.CAR_WITH_TRAFFIC.value
        )
        println(findRoute)
    }
}
