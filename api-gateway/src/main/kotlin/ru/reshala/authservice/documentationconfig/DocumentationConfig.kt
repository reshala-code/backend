package ru.reshala.authservice.documentationconfig

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.gateway.config.GatewayProperties
import org.springframework.cloud.gateway.route.RouteDefinition
import org.springframework.cloud.gateway.route.RouteDefinitionLocator
import org.springframework.cloud.gateway.route.RouteLocator
import org.springframework.cloud.gateway.support.NameUtils
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.ArrayList

import org.springframework.context.annotation.Primary
import org.springframework.stereotype.Component
import reactor.core.publisher.Flux
import springfox.documentation.swagger.web.SwaggerResource
import springfox.documentation.swagger.web.SwaggerResourcesProvider


@Configuration
class DocumentationConfig(val routeDefinitionLocator: RouteDefinitionLocator, val gatewayProperties: GatewayProperties) {


//        override fun get(): List<SwaggerResource?> {
//        val resources = mutableListOf<SwaggerResource?>()
//        var routes = mutableListOf<String?>()
//        gatewayProperties.routes.stream().filter { routeDefinition -> routes.contains(routeDefinition.getId()) }
//            .forEach { routeDefinition ->
//                routeDefinition.predicates.stream()
//                    .filter { predicateDefinition -> "Path".equals(predicateDefinition.getName(), ignoreCase = true) }
//                    .forEach { predicateDefinition ->
//                        resources.add(swaggerResource(routeDefinition.getId(),
//                            predicateDefinition.getArgs().get(NameUtils.GENERATED_NAME_PREFIX + "0")
//                                ?.replace("/**", "/v2/api-docs"), "2.0"))
//                    }
//            }
//        return resources
////        resources.add(swaggerResource("user-service", "/user/v2/api-docs", "2.0"))
////        resources.add(swaggerResource("notification-service", "/notification/v2/api-docs", "2.0"))
////        resources.add(swaggerResource("geo-service", "/geo/v2/api-docs", "2.0"))
////        resources.add(swaggerResource("task-service", "/task/v2/api-docs", "2.0"))
////        return resources
//    }
//
//    private fun swaggerResource(name: String, location: String?, version: String): SwaggerResource {
//        val swaggerResource = SwaggerResource()
//        swaggerResource.setName(name)
//        swaggerResource.setLocation(location)
//        swaggerResource.setSwaggerVersion(version)
//        return swaggerResource
//    }
    @Primary
    @Bean
    open fun swaggerResourcesProvider(): SwaggerResourcesProvider? {
        return SwaggerResourcesProvider {
            val resources: MutableList<SwaggerResource> = ArrayList()
            val definitions: Flux<RouteDefinition> = routeDefinitionLocator.routeDefinitions
            definitions
                .filter { routeDefinition: RouteDefinition ->
                            routeDefinition.id.endsWith("-service")
                }
                .subscribe { routeDefinition: RouteDefinition ->
                    resources.add(createResource(routeDefinition.id, "2.0"))
                }
            try {
                Thread.sleep(1000)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
            resources
        }
    }

    private fun createResource(location: String, version: String): SwaggerResource {
        val swaggerResource = SwaggerResource()
        swaggerResource.name = location
        swaggerResource.location = "/$location/v2/api-docs"
        swaggerResource.swaggerVersion = version
        return swaggerResource
    }

}
