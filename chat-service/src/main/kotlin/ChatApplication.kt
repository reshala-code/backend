package ru.reshala.chatservice

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration
import org.springframework.cloud.openfeign.EnableFeignClients

@EnableFeignClients
@SpringBootApplication()
class ChatApplication

fun main(args: Array<String>) {
    SpringApplication.run(ChatApplication::class.java, *args)
}
