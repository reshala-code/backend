package ru.reshala.chatservice.client

import org.springframework.cloud.openfeign.FeignClient
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import ru.reshala.chatservice.config.CustomDefaultFeignConfiguration
import java.time.LocalDateTime
import java.util.*
import javax.validation.constraints.Email


@Component
@FeignClient(name = "user-service", configuration = [CustomDefaultFeignConfiguration::class])
interface UserClient {

    @PostMapping("/user/users")
    fun findByUserIds(@RequestBody users: List<String>): Map<UUID, User>

}

data class User(
    val id: UUID,
    val firstName: String,
    val lastName: String,
    val photo: String?,
)
