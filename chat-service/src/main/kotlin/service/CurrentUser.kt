package ru.reshala.chatservice.service

import org.keycloak.KeycloakPrincipal
import org.keycloak.representations.AccessToken
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder

object CurrentUser {
    val userName: String
        get() {
            return getKeycloakToken().preferredUsername
        }

    val userId: String
        get() = getKeycloakToken().subject

    val token: AccessToken
        get() = getKeycloakToken()

    val userEmail: String
        get() = getKeycloakToken().phoneNumber

    val roles: List<out GrantedAuthority>?
        get() {
            return SecurityContextHolder.getContext().authentication.authorities.toList();
        }

    val isWorker: Boolean
        get() = CurrentUser.roles?.contains(SimpleGrantedAuthority("ROLE_WORKER"))?:false

    val isCustomer: Boolean
        get() = CurrentUser.roles?.contains(SimpleGrantedAuthority("ROLE_CUSTOMER"))?:false

    val isAdmin: Boolean
        get() = CurrentUser.roles?.contains(SimpleGrantedAuthority("ROLE_ADMIN"))?:false

    private fun getKeycloakToken(): AccessToken {
        return SecurityContextHolder.getContext().authentication?.principal?.let {
            (it as? KeycloakPrincipal<*>)?.keycloakSecurityContext?.token
                ?: throw Exception("Возникла ошибка при получении токена")
        }
            ?: throw Exception("Авторизованный пользователь не найден")
    }
}
