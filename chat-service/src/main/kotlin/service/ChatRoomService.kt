package ru.reshala.chatservice.service

import ru.reshala.chatservice.model.ChatRoom

import org.springframework.stereotype.Service
import ru.reshala.chatservice.repository.ChatRoomRepository
import java.util.*


@Service
class ChatRoomService(val chatRoomRepository: ChatRoomRepository) {
    /**
     * Создает чат-комнату и возвращает его Id
     */
    fun createChat(user1: String, user2: String): String? {
        val chatId = UUID.randomUUID()
        val chatRoom = ChatRoom(user1 = user1, user2 = user2)
        val rooms = getRoomByUserPair(user1, user2)
        if (rooms == null || rooms.isEmpty()) {
            val save = chatRoomRepository.save(chatRoom)
            return save.id
        }
        return rooms[0].id
    }

    fun getRoom(roomId: String): ChatRoom? {
        return chatRoomRepository.findOneById(roomId)
    }

    fun getRooms(userId:String): List<ChatRoom>? {
        return chatRoomRepository.findByUser1OrUser2(userId,userId)
    }

    fun getRoomByUserPair(user1Id: String, user2Id: String) : List<ChatRoom>?{
        val userPair = listOf(user1Id, user2Id)
        return chatRoomRepository.findByUser1InAndUser2In(userPair,userPair)
    }
}
