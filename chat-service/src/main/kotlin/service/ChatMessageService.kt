package ru.reshala.chatservice.service

import java.util.ArrayList

import org.springframework.data.mongodb.core.MongoOperations

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import ru.reshala.chatservice.model.ChatMessage
import ru.reshala.chatservice.model.ChatRoom
import ru.reshala.chatservice.repository.ChatMessageRepository
import ru.reshala.chatservice.service.ChatRoomService


@Service
class ChatMessageService(
    val repository: ChatMessageRepository,
    val chatRoomService: ChatRoomService,
) {

    fun save(chatMessage: ChatMessage): ChatMessage {
        chatRoomService.createChat(chatMessage.senderId!!, chatMessage.receiverId!!);
        repository.save(chatMessage)
        return chatMessage
    }

    fun getMessages(roomId: String): List<ChatMessage> {
        val room = chatRoomService.getRoom(roomId) ?: return emptyList()
        val user1 = room.user1!!
        val user2 = room.user2!!


        return repository.findBySenderIdInAndReceiverIdInOrderByCreatedDateTimeDesc(listOf(user1, user2),
            listOf(user1, user2))
    }

    fun getMessagesBetween(userId: String, receiverId: String): List<ChatMessage> {
        val roomByUserPair = chatRoomService.getRoomByUserPair(userId, receiverId)
        lateinit var room: ChatRoom
        if (roomByUserPair != null && roomByUserPair.size >= 1) {
            room = roomByUserPair[0];
        } else {
            return emptyList()
        }
        return getMessages(room.id!!)
    }

    fun getLastMessageInRoom(roomId: String): ChatMessage? {
        val room = chatRoomService.getRoom(roomId)
        if (room == null) return null

        return repository.findBySenderIdInAndReceiverIdInOrderByCreatedDateTimeDesc(listOf(room.user1!!, room.user2!!),
            listOf(room.user1!!, room.user2!!)).last();
    }

}
