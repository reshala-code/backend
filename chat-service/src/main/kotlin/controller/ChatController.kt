package ru.reshala.chatservice.controller


import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.springframework.web.bind.annotation.*
import ru.reshala.chatservice.client.Notification
import ru.reshala.chatservice.client.NotificationClient
import ru.reshala.chatservice.client.NotificationType
import ru.reshala.chatservice.client.UserClient
import ru.reshala.chatservice.dto.RoomDTO
import ru.reshala.chatservice.model.ChatMessage
import ru.reshala.chatservice.model.ChatRoom
import ru.reshala.chatservice.service.ChatMessageService
import ru.reshala.chatservice.service.ChatRoomService
import ru.reshala.chatservice.service.CurrentUser
import java.util.*


@RestController
@RequestMapping("/chat")
class ChatController(
    val chatMessageService: ChatMessageService,
    val chatRoomService: ChatRoomService,
    val notificationClient: NotificationClient,
    val userClient: UserClient,
) {
    @ApiOperation(value = "Получить сообщения в указанной комнате")
    @GetMapping("/room/{roomId}")
    fun getMessagesByRoomId(
        @ApiParam(required = true, value = "Уникальный идентификатор комнаты")
        @PathVariable roomId: String,
    ) {
        chatMessageService.getMessages(roomId)
    }

    @ApiOperation(value = "Получить сообщения по ID собеседника")
    @GetMapping("/{receiverId}")
    fun getMessages(
        @ApiParam(value = "ID собеседника или support", required = true)
        @PathVariable receiverId: String,
    ): List<ChatMessage> {
        if (CurrentUser.isAdmin) {
            return chatMessageService.getMessagesBetween("support", receiverId)
        }
        return chatMessageService.getMessagesBetween(CurrentUser.userId, receiverId)
    }

//    @ApiOperation("Получить сообщения с поддержкой")
//    @GetMapping("/support")
//    fun getMessages() {
//        chatMessageService.getMessagesBetween(CurrentUser.userId, "support")
//    }

    @ApiOperation("Отправить сообщение в чат с пользователем")
    @PostMapping("/message")
    fun sendMessage(
        @ApiParam("Объект сообщения ")
        @RequestBody chatMessage: ChatMessage,
    ): ChatMessage {
        chatMessage.senderName = CurrentUser.token.givenName
        if (CurrentUser.isAdmin){
            chatMessage.senderId = "support"
        }
        chatMessageService.save(chatMessage)
        val notification =
            Notification(chatMessage.receiverId.toString(), NotificationType.MESSAGE_RECEIVED, chatMessage)
        notificationClient.sendNotification(notification)
        return chatMessage;
    }


    @ApiOperation("Отправить сообщение в чат с поддержкой")
    @PostMapping("/support")
    fun sendMessageSupport(@RequestBody chatMessage: ChatMessage): ChatMessage {
        chatMessage.receiverId = "support"
        chatMessage.senderName = CurrentUser.token.givenName
        chatMessageService.save(chatMessage)
        val notification =
            Notification(chatMessage.receiverId.toString(), NotificationType.MESSAGE_RECEIVED, chatMessage)
        notificationClient.sendNotification(notification)
        return chatMessage;
    }

    @ApiOperation("Создать комнату чата")
    @PostMapping("/create")
    fun createChatRoom(@RequestParam user1: String, @RequestParam user2: String): String? {
        return chatRoomService.createChat(user1, user2)
    }

    @ApiOperation("Получить список комнат чата")
    @GetMapping("/rooms")
    fun getRooms(): List<RoomDTO>? {
        val userIdOrSupport = if (CurrentUser.isAdmin) "support" else CurrentUser.userId
        val rooms = chatRoomService.getRooms(userIdOrSupport)
        val users: MutableList<String> = mutableListOf()
        val roomDTOList = rooms?.map {
            val id = if (it.user1 == "support") it.user2 else it.user1
            if (id != null) {
                users.add(id);
                return@map RoomDTO(it.id, id)
            } else
                null;
        }?.filterNotNull()
        val userByIdMap = userClient.findByUserIds(users)
        roomDTOList?.forEach {
            val user = userByIdMap[UUID.fromString(it.userId)]
            if (user != null) {
                it.firstName = user.firstName
                it.lastName = user.lastName
                it.photo = user.photo;
                it.lastMessage = chatMessageService.getLastMessageInRoom(it.id!!)?.text
            }
        }
        return roomDTOList;
    }
}
