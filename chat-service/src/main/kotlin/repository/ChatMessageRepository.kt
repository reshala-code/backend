package ru.reshala.chatservice.repository

import org.springframework.data.mongodb.repository.MongoRepository
import ru.reshala.chatservice.model.ChatMessage

interface ChatMessageRepository : MongoRepository<ChatMessage?, String?> {
    fun findBySenderIdInAndReceiverIdInOrderByCreatedDateTimeDesc(list1: List<String>, list2: List<String>) : List<ChatMessage>
}
