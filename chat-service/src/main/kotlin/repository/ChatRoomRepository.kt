package ru.reshala.chatservice.repository

import org.springframework.data.mongodb.repository.MongoRepository
import ru.reshala.chatservice.model.ChatRoom


interface ChatRoomRepository : MongoRepository<ChatRoom?, String?> {
    fun findByUser1OrUser2(user1: String?, user2: String?): List<ChatRoom>?
    fun findOneById(roomId:String): ChatRoom?
    fun findByUser1InAndUser2In(users1:List<String>, users2: List<String> ) : List<ChatRoom>?

}
