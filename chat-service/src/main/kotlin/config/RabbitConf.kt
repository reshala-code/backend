package ru.reshala.chatservice.config

import com.rabbitmq.client.ConnectionFactory
import org.springframework.amqp.core.Binding
import org.springframework.amqp.core.BindingBuilder
import org.springframework.amqp.core.Queue
import org.springframework.amqp.core.TopicExchange
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter
import org.springframework.amqp.support.converter.MessageConverter
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer
import org.springframework.context.annotation.Bean

class RabbitConf {
//    @Bean
//    fun queue(): Queue? {
//        return Queue("chat", false)
//    }
//
//    @Bean
//    fun exchange(): TopicExchange? {
//        return TopicExchange("chat-exchange")
//    }
//
//    @Bean
//    fun binding(queue: Queue?, exchange: TopicExchange?): Binding? {
//        return BindingBuilder.bind(queue).to(exchange).with("tasks")
//    }
//
//    @Bean
//    fun jsaFactory(
//        connectionFactory: ConnectionFactory?,
//        configurer: SimpleRabbitListenerContainerFactoryConfigurer,
//    ): SimpleRabbitListenerContainerFactory? {
//        val factory = SimpleRabbitListenerContainerFactory()
//        configurer.configure(factory, connectionFactory)
//        factory.setMessageConverter(jsonMessageConverter())
//        return factory
//    }
//
////    @Bean
////    fun container(
////        connectionFactory: ConnectionFactory,
//////        listenerAdapter: MessageListenerAdapter,
////        messageListener: MessageListener,
////    ): SimpleMessageListenerContainer {
////
////        val container = SimpleMessageListenerContainer()
////        container.connectionFactory = connectionFactory
////        container.setQueueNames(queueName)
////        container.setMessageListener(messageListener)
////        return container
////    }
//
//    @Bean
//    fun jsonMessageConverter(): MessageConverter? {
//        return Jackson2JsonMessageConverter()
//    }
//
//
////    @Bean
////    fun listenerAdapter(receiver: TaskReceiver?): MessageListenerAdapter? {
////        return MessageListenerAdapter(receiver,"receiveMessage")
////    }
}
