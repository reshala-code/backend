package ru.reshala.chatservice.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.MongoId


@Document
class ChatRoom(
     @Id
     var id: String? = null,
     var user1: String? = null,
     var user2: String? = null
) {
}
