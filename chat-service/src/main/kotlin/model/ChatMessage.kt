package ru.reshala.chatservice.model

import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.MongoId
import java.time.LocalDateTime
import java.time.ZoneOffset

@Document
data class ChatMessage(
    @MongoId
    var id: String?,
    var text: String?,
    var imageUrl: String?,
    var senderId: String?,
    var receiverId: String?,
    var senderName: String?,
    var receiverName: String?,
    var roomID: String?,
    var relatedTaskId: String?,
    var createdDate: LocalDateTime = LocalDateTime.now(),

    )
