package ru.reshala.chatservice.dto

class RoomDTO(
    val id: String?=null,
    var userId: String?=null,
    var lastMessage: String?=null,
    var messageType: String?=null,
    var firstName: String?=null,
    var lastName: String?=null,
    var photo: String?=null
)
