dependencies{
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.cloud:spring-cloud-starter-netflix-eureka-client"){
        exclude("javax.ws.rs","jsr311-api")
    }
    implementation("org.springframework.cloud:spring-cloud-starter-security")
    implementation("org.keycloak:keycloak-spring-boot-starter:12.0.4")
    implementation("org.keycloak:keycloak-admin-client:12.0.4")
    implementation("org.springframework.boot:spring-boot-starter-oauth2-resource-server")
    implementation("org.springframework.boot:spring-boot-starter-oauth2-client")
    implementation("org.springframework.security:spring-security-oauth2-jose")
    implementation("org.springframework.boot:spring-boot-starter-data-mongodb")

    implementation("org.springframework.boot:spring-boot-starter-amqp")



}
